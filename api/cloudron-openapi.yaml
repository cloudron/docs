openapi: 3.1.0

info:
  title: Cloudron API
  version: 1.0.0
  contact:
    url: https://forum.cloudron.io
  x-logo:
    url: "/img/cloudron-banner.png"
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  # cannot move the whole description to a ref - https://github.com/OAI/OpenAPI-Specification/issues/1570
  # for security section injection - https://redocly.com/docs/redoc/security-definitions-injection/
  description: |
    # Introduction

    Cloudron provides a RESTful API to manage apps, users, groups, domains and other resources.

    All of the functionality in the Cloudron Dashboard is available through the API, allowing you to script complex actions.

    If you are an app developer, the [Cloudron CLI tool](https://www.npmjs.com/package/cloudron) implements a workflow that allows
    you to develop apps on your Cloudron. The CLI tool uses the REST API documented here.

    ## Requests

    The API follows standard HTTP REST API conventions.

    |Method|Usage|
    |--- |--- |
    |GET| Retrieveal operations |
    |DELETE| Destroys a resource |
    |POST| Create new objects or start a new task. Also used to update parts of a resource |
    |PUT| Idempotent operation that replaces a resource |
    |PATCH| API does not support PATCH |
    |HEAD| Read response headers and get no response |

    ## HTTP Statuses

    The API follows standard HTTP REST API response conventions.

    2xx responses indicate success. 4xx responses indicate request errors. 5xx responses indicate server errors.

    |Name|Description|
    |--- |--- |
    |200 | Success. Operation succeeded, response body contains more information |
    |204 | Success. Operation succeeded, empty response body |
    |401 | Access Denied. Usually caused by missing, invalid or expired token|
    |403 | Forbidden. Authenticated but operation not permitted. Usually caused by insufficient role/permissions |
    |5xx | Internal Server error |

    ## Pagination

    When listing objects, query parameters `page` and `per_page` can be passed in. Note that these parameters have index
    starting from 1.

    ## API Token

    To use the REST API, you must first create an [API token](https://docs.cloudron.io/profile/#api-tokens). This token can be provided
    in two ways:
      * Via the request query parameter `?access_token=<token>`
      * Via the Authorization header `Bearer <token>`

    API Tokens can be created as `Readonly` or `Read and Write`. With `Readonly` token, only GET operations can be performed.

    ## cURL Examples

    We use `curl` to demonstrate how to use the API. The Cloudron Dashboard Domain and the API Token are variable fields.
    You can export them in your shell as follows:

    ```
    export CLOUDRON_DOMAIN=my.domain.com
    export CLOUDRON_TOKEN=your_token_here
    ```

    To list the domains using cURL and the `Authorization` header:

    ```
      curl -H 'ContentType: application/json' -H "Authorization: Bearer $CLOUDRON_TOKEN" https://$CLOUDRON_DOMAIN/api/v1/domains
    ```

    Alternately, to use the query parameter:

    ```
      curl -H 'ContentType: application/json' "https://$CLOUDRON_DOMAIN/api/v1/domains?access_token=$CLOUDRON_TOKEN"
    ```

    # Authorization

    <!-- Redoc-Inject: <security-definitions> -->

# when you click the routes on the right, this will appear
servers:
  - url: https://{cloudron_domain}/api/v1
    description: Cloudron API
    variables:
      cloudron_domain:
        default: my.cloudron.example
        description: The Cloudron Dashboard Domain

components:
  securitySchemes:
    bearer_auth:
      type: http
      scheme: bearer
      description: >
        To use the Bearer Authentication, you must first create an [API token](https://docs.cloudron.io/profile/#api-tokens).

        The API Token can be passed via the Authorization header `Bearer <token>`:

        ```
          curl -H 'ContentType: application/json' -H 'Authorization: Bearer $CLOUDRON_TOKEN' https://$CLOUDRON_DOMAIN/api/v1/domains
        ```

    query_auth:
      type: apiKey
      name: access_token
      in: query
      description: >
        To use the Query Parameter Authentication, you must first create an [API token](https://docs.cloudron.io/profile/#api-tokens).

        The API Token can be passed via the  `access_token` query parameter:

        ```
          curl -H 'ContentType: application/json' 'https://$CLOUDRON_DOMAIN/api/v1/domains?access_token=$CLOUDRON_TOKEN'
        ```

security:
  - bearer_auth: []
  - query_auth: []

# This controls listing of the API groups
tags:
  - name: Apps
    description: Handle your cloudron apps using the apps api.
  - name: Applinks
    description: Applinks are apps that you externally added to your app overview.
  - name: App Passwords
    description: >
      App passwords can be used as a security measure in desktop, email & mobile clients. For example, if you are trying out a new mobile app
      from an untrusted vendor, you can generate a temporary password that provides access to a specific app. This way your main password
      does not get compromised (and thus providing access to other apps as well).
  - name: Backups
    description: Handling the backups.
  - name: Branding
    description: The Branding configuration can be used to customize various aspects of the Cloudron like it's name, logo and footer.
  - name: Cloudron
    description: Cloudron Healthcheck route and various global configuration like language & timezone can be found here.
  - name: Dashboard
    description: Configuration of your cloudron server.
  - name: Docker
    description: Docker related configuration.
  - name: Domains
    description: Web addresses for accessing your server and apps. Read more about Cloudron domain settings in our [knowledgebase](https://docs.cloudron.io/domains/).
  - name: Directory Server
    description: Directory Server Configuration
  - name: Eventlog
    description: Cloudron server activity can be monitored using the Eventlog API.
  - name: External LDAP
    description: The External Directory connector allows users from your existing LDAP or active directory to authenticate with Cloudron.
  - name: Groups
    description: Groups provide a convenient way to group users. You can assign one or more groups to apps to restrict who can access for an app.
  - name: Mail
    description: Configuration for your mail domains.
  - name: Mailserver
    description: Configuration, Logs and Information about the mailserver.
  - name: Network
    description: Networking and Firewall related configuration.
  - name: Notifications
    description: >
      Cloudron displays notifications in the dashboard for various events like: app down, app out of memory, low disk space, updates available,
      app update etc. The notifications can be read by clicking on the bell icon in the navigation bar.
  - name: OIDC
    description: >
      OpenID Connect. It allows clients to verify the identity of end-users based on the authentication performed by an authorization server.
  - name: Profile
    description: User's profile information
  - name: Provision
    description: This endpoint allows users to provision and configure domain settings. It validates and applies various configurations such as provider details, domain name, zone information, TLS settings, and optional IPv4 and IPv6 configurations.
  - name: Reverse Proxy
    description: The Reverse Proxy manages the nginx config and TLS certificates.
  - name: Services
    description: Services that are used on your Cloudron instance such as 'mail', 'mysql' and way more.
  - name: System
    description: Server related information.
  - name: Tasks
    description: Tasks are asynchronous operations performed in the background. Each task can be monitored separately using the task id.
  - name: Tokens
    description: Tokens are used for API authentication.
  - name: Updater
    description: Updater checks and manages updates of the apps and the platform.
  - name: Users
    description: Add, update and remove users from Cloudron User Directory.
  - name: User Directory
    description: Settings & Configuration for [Cloudron User Directory](https://docs.cloudron.io/user-directory/).
  - name: Volumes
    description: Definition of storage volumes including their properties and configurations.

paths:
  /apps:
    $ref: 'paths/apps.yaml#/~1apps'
  /apps/{appId}:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}'
  /apps/{appId}/icon:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1icon'
  /apps/{appId}/uninstall:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1uninstall'
  /apps/{appId}/repair:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1repair'
  /apps/{appId}/check_for_updates:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1check_for_updates'
  /apps/{appId}/update:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1update'
  /apps/{appId}/restore:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1restore'
  /apps/{appId}/import:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1import'
  /apps/{appId}/export:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1export'
  /apps/{appId}/start:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1start'
  /apps/{appId}/stop:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1stop'
  /apps/{appId}/restart:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1restart'
  /apps/{appId}/logstream:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1logstream'
  /apps/{appId}/logs:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1logs'
  /apps/{appId}/eventlog:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1eventlog'
  /apps/{appId}/task:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1task'
  /apps/{appId}/clone:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1clone'
  #/apps/{appId}/download:
  #  $ref: 'paths/apps.yaml#/~1apps~1{appId}~1download'
  #/apps/{appId}/upload:
  #  $ref: 'paths/apps.yaml#/~1apps~1{appId}~1upload'
  /apps/{appId}/exec:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1exec'
  #/apps/{appId}/exec/{execId}/start:
  #  $ref: 'paths/apps.yaml#/~1apps~1{appId}~1exec~1{execId}~1start'
  #/apps/{appId}/exec/{execId}/startws:
  #  $ref: 'paths/apps.yaml#/~1apps~1{appId}~1exec~1{execId}~1startws'
  /apps/{appId}/configure/access_restriction:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1access_restriction'
  /apps/{appId}/configure/operators:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1operators'
  /apps/{appId}/configure/label:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1label'
  /apps/{appId}/configure/tags:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1tags'
  /apps/{appId}/configure/icon:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1icon'
  /apps/{appId}/configure/notes:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1notes'
  /apps/{appId}/configure/memory_limit:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1memory_limit'
  /apps/{appId}/configure/cpu_quota:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1cpu_quota'
  /apps/{appId}/configure/automatic_backup:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1automatic_backup'
  /apps/{appId}/configure/automatic_update:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1automatic_update'
  /apps/{appId}/configure/reverse_proxy:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1reverse_proxy'
  /apps/{appId}/configure/cert:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1cert'
  /apps/{appId}/configure/debug_mode:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1debug_mode'
  /apps/{appId}/configure/mailbox:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1mailbox'
  /apps/{appId}/configure/inbox:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1inbox'
  /apps/{appId}/configure/turn:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1turn'
  /apps/{appId}/configure/redis:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1redis'
  /apps/{appId}/configure/env:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1env'
  /apps/{appId}/configure/storage:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1storage'
  /apps/{appId}/configure/location:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1location'
  /apps/{appId}/configure/mounts:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1mounts'
  /apps/{appId}/configure/crontab:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1crontab'
  /apps/{appId}/configure/upstream_uri:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1configure~1upstream_uri'
  /apps/{appId}/backups:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1backups'
  /apps/{appId}/backups/{backupId}:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1backups~1{backupId}'
  /apps/{appId}/backups/{backupId}/download:
    $ref: 'paths/apps.yaml#/~1apps~1{appId}~1backups~1{backupId}~1download'

  /applinks:
    $ref: 'paths/applinks.yaml#/~1applinks'
  /applinks/{id}:
    $ref: 'paths/applinks.yaml#/~1applinks~1{id}'
  /applinks/{id}/icon:
    $ref: 'paths/applinks.yaml#/~1applinks~1{id}~1icon'
  /app_passwords:
    $ref: 'paths/app-passwords.yaml#/~1app_passwords'
  /app_passwords/{passwordId}:
    $ref: 'paths/app-passwords.yaml#/~1app_passwords~1{passwordId}'

  /backups:
    $ref: 'paths/backups.yaml#/~1backups'
  /backups/mount_status:
    $ref: 'paths/backups.yaml#/~1backups~1mount_status'
  /backups/create:
    $ref: 'paths/backups.yaml#/~1backups~1create'
  /backups/cleanup:
    $ref: 'paths/backups.yaml#/~1backups~1cleanup'
  /backups/remount:
    $ref: 'paths/backups.yaml#/~1backups~1remount'
  /backups/config:
    $ref: 'paths/backups.yaml#/~1backups~1config'
  /backups/config/storage:
    $ref: 'paths/backups.yaml#/~1backups~1config~1storage'
  /backups/policy:
    $ref: 'paths/backups.yaml#/~1backups~1policy'
  /backups/{backupId}:
    $ref: 'paths/backups.yaml#/~1backups~1{backupId}'

  /branding/cloudron_avatar:
    $ref: 'paths/branding.yaml#/~1branding~1cloudron_avatar'
  /branding/cloudron_name:
    $ref: 'paths/branding.yaml#/~1branding~1cloudron_name'
  /branding/cloudron_background:
    $ref: 'paths/branding.yaml#/~1branding~1cloudron_background'
  /branding/footer:
    $ref: 'paths/branding.yaml#/~1branding~1footer'

  /cloudron/status:
    $ref: 'paths/cloudron.yaml#/~1cloudron~1status'
  /cloudron/avatar:
    $ref: 'paths/cloudron.yaml#/~1cloudron~1avatar'
  /cloudron/background:
    $ref: 'paths/cloudron.yaml#/~1cloudron~1background'
  /cloudron/languages:
    $ref: 'paths/cloudron.yaml#/~1cloudron~1languages'
  /cloudron/language:
    $ref: 'paths/cloudron.yaml#/~1cloudron~1language'
  /cloudron/time_zone:
    $ref: 'paths/cloudron.yaml#/~1cloudron~1time_zone'

  /dashboard/config:
    $ref: 'paths/dashboard.yaml#/~1dashboard~1config'
  /dashboard/startPrepareLocation:
    $ref: 'paths/dashboard.yaml#/~1dashboard~1startPrepareLocation'
  /dashboard/location:
    $ref: 'paths/dashboard.yaml#/~1dashboard~1location'

  /directory_server/config:
    $ref: 'paths/directory-server.yaml#/~1directory_server~1config'

  /docker/registry_config:
    $ref: 'paths/docker.yaml#/~1docker~1registry_config'

  /domains:
    $ref: 'paths/domains.yaml#/~1domains'
  /domains/sync_dns:
    $ref: 'paths/domains.yaml#/~1domains~1sync_dns'
  /domains/{domain}:
    $ref: 'paths/domains.yaml#/~1domains~1{domain}'
  /domains/{domain}/config:
    $ref: 'paths/domains.yaml#/~1domains~1{domain}~1config'
  /domains/{domain}/wellknown:
    $ref: 'paths/domains.yaml#/~1domains~1{domain}~1wellknown'
  /domains/{domain}/dns_check:
    $ref: 'paths/domains.yaml#/~1domains~1{domain}~1dns_check'

  /eventlog:
    $ref: 'paths/eventlog.yaml#/~1eventlog'
  /eventlog/{eventId}:
    $ref: 'paths/eventlog.yaml#/~1eventlog~1{eventId}'

  /external_ldap/config:
    $ref: 'paths/external-ldap.yaml#/~1external_ldap~1config'
  /external_ldap/sync:
    $ref: 'paths/external-ldap.yaml#/~1external_ldap~1sync'

  /mail/{domain}:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}'
  /mail/{domain}/lists:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1lists'
  /mail/{domain}/lists/{name}:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1lists~1{name}'
  /mail/{domain}/enable:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1enable'
  /mail/{domain}/status:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1status'
  /mail/{domain}/mail_from_validation:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1mail_from_validation'
  /mail/{domain}/catch_all:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1catch_all'
  /mail/{domain}/relay:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1relay'
  /mail/{domain}/banner:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1banner'
  /mail/{domain}/send_test_mail:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1send_test_mail'
  /mail/{domain}/mailbox_count:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1mailbox_count'
  /mail/{domain}/mailboxes:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1mailboxes'
  /mail/{domain}/mailboxes/{name}:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1mailboxes~1{name}'
  /mail/{domain}/mailboxes/{name}/aliases:
    $ref: 'paths/mail.yaml#/~1mail~1{domain}~1mailboxes~1{name}~1aliases'

  /mailserver/eventlog:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1eventlog'
  /mailserver/clear_eventlog:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1clear_eventlog'
  /mailserver/location:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1location'
  /mailserver/max_email_size:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1max_email_size'
  /mailserver/spam_acl:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1spam_acl'
  /mailserver/spam_custom_config:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1spam_custom_config'
  /mailserver/dnsbl_config:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1dnsbl_config'
  /mailserver/solr_config:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1solr_config'
  /mailserver/mailbox_sharing:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1mailbox_sharing'
  /mailserver/virtual_all_mail:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1virtual_all_mail'
  /mailserver/usage:
    $ref: 'paths/mailserver.yaml#/~1mailserver~1usage'

  /groups:
    $ref: 'paths/groups.yaml#/~1groups'
  /groups/{groupId}:
    $ref: 'paths/groups.yaml#/~1groups~1{groupId}'
  /groups/{groupId}/members:
    $ref: 'paths/groups.yaml#/~1groups~1{groupId}~1members'
  /groups/{groupId}/name:
    $ref: 'paths/groups.yaml#/~1groups~1{groupId}~1name'

  /network/blocklist:
    $ref: 'paths/network.yaml#/~1network~1blocklist'
  /network/dynamic_dns':
    $ref: 'paths/network.yaml#/~1network~1dynamic_dns'
  /network/ipv4_config:
    $ref: 'paths/network.yaml#/~1network~1ipv4_config'
  /network/ipv6_config:
    $ref: 'paths/network.yaml#/~1network~1ipv6_config'
  /network/ipv4:
    $ref: 'paths/network.yaml#/~1network~1ipv4'
  /network/ipv6:
    $ref: 'paths/network.yaml#/~1network~1ipv6'

  /notifications:
    $ref: 'paths/notifications.yaml#/~1notifications'
  /notifications/{notificationId}:
    $ref: 'paths/notifications.yaml#/~1notifications~1{notificationId}'

  /oidc/clients:
    $ref: 'paths/oidc.yaml#/~1oidc~1clients'
  /oidc/clients/{clientId}:
    $ref: 'paths/oidc.yaml#/~1oidc~1clients~1{clientId}'
  /oidc/sessions:
    $ref: 'paths/oidc.yaml#/~1oidc~1sessions'

  /profile:
    $ref: 'paths/profile.yaml#/~1profile'
  /profile/avatar:
    $ref: 'paths/profile.yaml#/~1profile~1avatar'
  /profile/avatar/{userId}:
    $ref: 'paths/profile.yaml#/~1profile~1avatar~1{userId}'
  /profile/background_image:
    $ref: 'paths/profile.yaml#/~1profile~1background_image'
  /profile/password:
    $ref: 'paths/profile.yaml#/~1profile~1password'
  /profile/twofactorauthentication_secret:
    $ref: 'paths/profile.yaml#/~1profile~1twofactorauthentication_secret'
  /profile/twofactorauthentication_enable:
    $ref: 'paths/profile.yaml#/~1profile~1twofactorauthentication_enable'
  /profile/twofactorauthentication_disable:
    $ref: 'paths/profile.yaml#/~1profile~1twofactorauthentication_disable'

  /provision/setup:
    $ref: 'paths/provision.yaml#/~1provision~1setup'
  /provision/restore:
    $ref: 'paths/provision.yaml#/~1provision~1restore'
  /provision/activate:
    $ref: 'paths/provision.yaml#/~1provision~1activate'

  /reverseproxy/renew_certs:
    $ref: 'paths/reverseproxy.yaml#/~1reverseproxy~1renew_certs'
  /reverseproxy/trusted_ips:
    $ref: 'paths/reverseproxy.yaml#/~1reverseproxy~1trusted_ips'

  /services:
    $ref: 'paths/services.yaml#/~1services'
  /services/platform_status:
    $ref: 'paths/services.yaml#/~1services~1platform_status'
  /services/{service}:
    $ref: 'paths/services.yaml#/~1services~1{service}'
  /services/{service}/graphs:
    $ref: 'paths/services.yaml#/~1services~1{service}~1graphs'
  /services/{service}/logs:
    $ref: 'paths/services.yaml#/~1services~1{service}~1logs'
  /services/{service}/logstream:
    $ref: 'paths/services.yaml#/~1services~1{service}~1logstream'
  /services/{service}/restart:
    $ref: 'paths/services.yaml#/~1services~1{service}~1restart'
  /services/{service}/rebuild:
    $ref: 'paths/services.yaml#/~1services~1{service}~1rebuild'

  /system/info:
    $ref: 'paths/system.yaml#/~1system~1info'
  /system/reboot:
    $ref: 'paths/system.yaml#/~1system~1reboot'
  /system/cpus:
    $ref: 'paths/system.yaml#/~1system~1cpus'
  /system/disk_usage:
    $ref: 'paths/system.yaml#/~1system~1disk_usage'
  /system/block_devices:
    $ref: 'paths/system.yaml#/~1system~1block_devices'
  /system/memory:
    $ref: 'paths/system.yaml#/~1system~1memory'
  /system/logs/{unit}:
    $ref: 'paths/system.yaml#/~1system~1logs~1{unit}'
  /system/logstream/{unit}:
    $ref: 'paths/system.yaml#/~1system~1logstream~1{unit}'

    # router.get ('/api/v1/system/graphs',                         token, authorizeAdmin, routes.system.getSystemGraphs);
    # router.get ('/api/v1/system/disks',                          token, authorizeAdmin, routes.system.getDisks);
    # router.get ('/api/v1/system/block_devices',                  token, authorizeAdmin, routes.system.getBlockDevices);
    # router.get ('/api/v1/system/memory',                         token, authorizeAdmin, routes.system.getMemory);
    # router.get ('/api/v1/system/logs/:unit',                     token, authorizeAdmin, routes.system.getLogs);
    # router.get ('/api/v1/system/logstream/:unit',                token, authorizeAdmin, routes.system.getLogStream);

  /tasks:
    $ref: 'paths/tasks.yaml#/~1tasks'
  /tasks/{taskId}:
    $ref: 'paths/tasks.yaml#/~1tasks~1{taskId}'
  /tasks/{taskId}/logs:
    $ref: 'paths/tasks.yaml#/~1tasks~1{taskId}~1logs'
  /tasks/{taskId}/logstream:
    $ref: 'paths/tasks.yaml#/~1tasks~1{taskId}~1logstream'
  /tasks/{taskId}/stop:
   $ref: 'paths/tasks.yaml#/~1tasks~1{taskId}~1stop'

  /tokens:
      $ref: 'paths/tokens.yaml#/~1tokens'
  /tokens/{tokenId}:
    $ref: 'paths/tokens.yaml#/~1tokens~1{tokenId}'

  /updater/updates:
    $ref: 'paths/updater.yaml#/~1updater~1updates'
  /updater/check_for_updates:
    $ref: 'paths/updater.yaml#/~1updater~1check_for_updates'
  /updater/update:
    $ref: 'paths/updater.yaml#/~1updater~1update'
  /updater/autoupdate_pattern:
    $ref: 'paths/updater.yaml#/~1updater~1autoupdate_pattern'

  /users:
    $ref: 'paths/users.yaml#/~1users'
  /users/{userId}:
    $ref: 'paths/users.yaml#/~1users~1{userId}'
  /users/{userId}/role:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1role'
  /users/{userId}/active:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1active'
  /users/{userId}/profile:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1profile'
  /users/{userId}/password:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1password'
  /users/{userId}/ghost:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1ghost'
  /users/{userId}/groups:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1groups'
  /users/{userId}/password_reset_link:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1password_reset_link'
  /users/{userId}/send_password_reset_email:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1send_password_reset_email'
  /users/{userId}/invite_link:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1invite_link'
  /users/{userId}/send_invite_email:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1send_invite_email'
  /users/{userId}/twofactorauthentication_disable:
    $ref: 'paths/users.yaml#/~1users~1{userId}~1twofactorauthentication_disable'

  /user_directory:
    $ref: 'paths/user-directory.yaml#/~1user_directory~1profile_config'

  /volumes:
    $ref: 'paths/volumes.yaml#/~1volumes'
  /volumes/{id}:
    $ref: 'paths/volumes.yaml#/~1volumes~1{id}'
  /volumes/{id}/status:
    $ref: 'paths/volumes.yaml#/~1volumes~1{id}~1status'
  /volumes/{id}/remount:
    $ref: 'paths/volumes.yaml#/~1volumes~1{id}~1remount'
  /volumes/{id}/files/{filename}:
    $ref: 'paths/volumes.yaml#/~1volumes~1{id}~1files~1{filename}'




