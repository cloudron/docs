definitions:
# Volume: Is the object having all the properties in one for the return of 'GET [...]/api/v1/volumes'
  Volume:
    type: object
    description: Volume 
    properties:
      id:
        type: string
        description: Unique identifier for the volume.
        example: "1af5f811fddb4337ac5f71b0989c1d30"
      name:
        type: string
        description: Name of the volume.
        example: "Main Volume"
      hostPath:
        type: string
        description: Path on host machine where the volume is located.
        example: "/mnt/volumes/"
      creationTime:
        type: string
        format: date-time
        description: Date and time when the volume was created.
        example: "2024-03-20T18:00:48.000Z"
      mountType:
        type: string
        description: >
          Indicates the method or protocol used to mount a volume, defining how the volume is accessed and interacted with within the system.
          See more about [mount types](https://docs.cloudron.io/volumes/#mount-type).
            * `cifs`: Network file system protocol commonly used for sharing files and printers in a network.
            * `filesystem`: Refers to the type of volume that is mounted directly from the local filesystem.
            * `ext4`: A filesystem type recognized for its performance, scalability, and support for large file sizes and partitions.
            * `mountpoint`: Represents a location in the filesystem hierarchy where a volume is mounted.
            * `nfs`: Network File System, a distributed file system protocol allowing a user on a client computer to access files over a network as if the files were locally stored.
            * `sshfs`: SSH Filesystem, a network file system protocol built on the Secure Shell protocol.
            * `xfs`: Another type of filesystem known for its scalability and high-performance features.
        enum: [cifs, filesystem, ext4, mountpoint, nfs, sshfs, xfs]
        example: cifs
      mountOptions:
            type: object
            description: The mountOptions vary based on the chosen mountType. The given example is for the mountType `cifs`
            properties:
              host:
                type: string
                description: Host address (if applicable).
                example: "example.com"
              remoteDir:
                type: string
                description: Remote directory for network-mounted volumes.
                example: "/backup"
              username:
                type: string
                description: Username for accessing the volume (if applicable).
                example: "u328893"
              password:
                type: string
                description: Password for accessing the volume (if applicable).
                example: "your_password"
              seal:
                type: boolean
                description: Indicates whether the volume is sealed.
                example: true

# volumeBase: Is used for the properties that are part of the 'POST Calls' where the data structure changes based on the mountType.   
  volumeBase:
    required:
      - name
      - mountType
    type: object
    description: Specifies the name and mountType of the volume.
    properties:
      name:
        type: string
        description: A valid name for the volume should consist of printable characters and valid filesystem characters. This includes letters, numbers, spaces, and various symbols commonly accepted in filesystem naming conventions.
        example: "Main Volume"
      mountType:
        type: string
        description: >
          Indicates the method or protocol used to mount a volume, defining how the volume is accessed and interacted with within the system. 
          See more about [mount types](https://docs.cloudron.io/volumes/#mount-type).
            * `cifs`: Network file system protocol commonly used for sharing files and printers in a network.
            * `filesystem`: Refers to the type of volume that is mounted directly from the local filesystem.
            * `ext4`: A filesystem type recognized for its performance, scalability, and support for large file sizes and partitions.
            * `mountpoint`: Represents a location in the filesystem hierarchy where a volume is mounted.
            * `nfs`: Network File System, a distributed file system protocol allowing a user on a client computer to access files over a network as if the files were locally stored.
            * `sshfs`: SSH Filesystem, a network file system protocol built on the Secure Shell protocol.
            * `xfs`: Another type of filesystem known for its scalability and high-performance features.
        enum: [cifs, filesystem, ext4, mountpoint, nfs, sshfs, xfs]
        example: cifs 

# cifsMountOptions: Is used to have an option to only show the mountOptions. (Used for the 'Update Volume' call)
  cifsMountOptions:
    type: object
    properties:
      mountOptions:
        type: object
        description: Specifies extra settings for mounting a filesystem. Needed properties vary based on the mountType.
        properties:
          host:
            type: string
            description: Host address (if applicable).
            example: "example.com"
          remoteDir:
            type: string
            description: Remote directory for network-mounted volumes.
            example: "/backup"
          username:
            type: string
            description: Username for accessing the volume (if applicable).
            example: "u328893"
          password:
            type: string
            description: Password for accessing the volume (if applicable).
            example: "your_password"
          seal:
            type: boolean
            description: Indicates whether the volume is sealed.
            example: true
        required:
          - host
          - remoteDir
          - username
          - password
          - seal
    required:
      - mountOptions

# CIFS: Is used to adjust the payload and request body when the mountType is.
  CIFS:
    allOf:
      - $ref: '#/definitions/volumeBase'
      - type: object
        properties:  
            $ref: '#/definitions/cifsMountOptions/properties'
        required:
          - mountOptions

# filesystemMountOptions: Is used to have an option to only show the mountOptions. (Used for the 'Update Volume' call)
  filesystemMountOptions:
    type: object
    properties:
      mountOptions:
        type: object
        description: Specifies extra settings for mounting a filesystem. Needed properties vary based on the mountType.
        properties:
          hostPath:
            type: string
            description: Path on the host machine (if applicable).
            example: "/srv/shared"
        required:
          - hostPath
    required:
      - mountOptions

# FILESYSTEM: Is used to adjust the payload and request body when the mountType is.
  FILESYSTEM:
    allOf:
      - $ref: '#/definitions/volumeBase'
      - type: object
        properties:
            $ref: '#/definitions/filesystemMountOptions/properties'
        required:
          - mountOptions

# ext4MountOptions: Is used to have an option to only show the mountOptions. (Used for the 'Update Volume' call)
  ext4MountOptions:
    type: object
    properties:
      mountOptions:
        type: object
        description: Specifies extra settings for mounting a filesystem. Needed properties vary based on the mountType.
        properties:
          diskPath:
            type: string
            example: "YourDiskPath"
            description: Specifies the path to the disk to be mounted.
        required:
          - diskPath
    required:
      - mountOptions

# EXT4: Is used to adjust the payload and request body when the mountType is.
  EXT4:
    allOf:
      - $ref: '#/definitions/volumeBase'
      - type: object
        properties:
            $ref: '#/definitions/ext4MountOptions/properties'
        required:
          - mountOptions

# mountpointMountOptions: Is used to have an option to only show the mountOptions. (Used for the 'Update Volume' call)            
  mountpointMountOptions:
    type: object
    properties:
      mountOptions:
        type: object
        description: Specifies extra settings for mounting a filesystem. Needed properties vary based on the mountType.
        properties:
          hostPath:
            type: string
            description: Path on the host machine (if applicable).
            example: "/srv/shared"
        required:
          - hostPath
    required:
      - mountOptions

# MOUNTPOINT: Is used to adjust the payload and request body when the mountType is.
  MOUNTPOINT:
    allOf:
      - $ref: '#/definitions/volumeBase'
      - type: object
        properties:
          $ref: '#/definitions/mountpointMountOptions/properties'
        required:
          - mountOptions

# nfsMountOptions: Is used to have an option to only show the mountOptions. (Used for the 'Update Volume' call)            
  nfsMountOptions:
    type: object
    properties:
      mountOptions:
        type: object
        description: Specifies extra settings for mounting a filesystem. Needed properties vary based on the mountType.
        properties:
          host:
            type: string
            example: "your.volume.host.com"
            description: Specifies the hostname of the NFS server.
          remoteDir:
            type: string
            example: /share
            description: Specifies the directory path on the server to be mounted.
        required:
          - host
          - remoteDir
    required:
      - mountOptions

# NFS: Is used to adjust the payload and request body when the mountType is.
  NFS:
    allOf:
      - $ref: '#/definitions/volumeBase'
      - type: object
        properties:
            $ref: '#/definitions/nfsMountOptions/properties/'
        required:
          - mountOptions
          
# sshfsMountOptions: Is used to have an option to only show the mountOptions. (Used for the 'Update Volume' call)            
  sshfsMountOptions:
    type: object
    properties:
      mountOptions:
        type: object
        description: Specifies extra settings for mounting a filesystem. Needed properties vary based on the mountType.
        properties:
          host:
            type: string
            example: "your.volume.host.com"
            description: Specifies the hostname of the SSHFS server.
          remoteDir:
            type: string
            example: /share
            description: Directory path on the server.
          port:
            type: number
            example: 9876
            description: Port number for the connection.
          user:
            type: string
            example: "Volume User"
            description: Username for authentication.
          privateKey:
            type: string
            example: "SSH_PRIVATE_KEY"
            description: Key for authentication.
        required:
          - host
          - remoteDir
          - port
          - user
          - privateKey
    required:
      - mountOptions

# SSHFS: Is used to adjust the payload and request body when the mountType is. 
  SSHFS:
    allOf:
      - $ref: '#/definitions/volumeBase'
      - type: object
        properties:
            $ref: '#/definitions/sshfsMountOptions/properties'
        required:
          - mountOptions

# xfsMountOptions: Is used to have an option to only show the mountOptions. (Used for the 'Update Volume' call)            
  xfsMountOptions:
    type: object
    properties:
      mountOptions:
          type: object
          description: Specifies extra settings for mounting a filesystem. Needed properties vary based on the mountType.
          properties:
            diskPath:
              type: string
              example: "YourDiskPath"
              description: Specifies the path to the disk to be mounted.
          required: 
            - diskPath

# XFS: Is used to adjust the payload and request body when the mountType is.                   
  XFS:
    allOf:
    - $ref: '#/definitions/volumeBase'
    - type: object
      properties:
        $ref: '#/definitions/xfsMountOptions/properties'
      required:
        - mountOptions

# volumeByMountType
  volumeByMountType:
    type: object
    discriminator:
      propertyName: mountType
      mapping:
        cifs: '#/definitions/CIFS'
        filesystem: '#/definitions/FILESYSTEM'
        ext4: '#/definitions/EXT4'
        mountpoint: '#/definitions/MOUNTPOINT'
        nfs: '#/definitions/NFS'
        sshfs: '#/definitions/SSHFS'
        xfs: '#/definitions/XFS'
    oneOf:
      - $ref: '#/definitions/CIFS'
      - $ref: '#/definitions/FILESYSTEM'
      - $ref: '#/definitions/EXT4'
      - $ref: '#/definitions/MOUNTPOINT'
      - $ref: '#/definitions/NFS'
      - $ref: '#/definitions/SSHFS'
      - $ref: '#/definitions/XFS'

# Parameters for the "id" needed in the path. [Example: /volume/{id}]
parameters:
  id:
    name: id
    in: path
    description: Volume ID
    required: true
    schema:
      type: string
  
  filename:
    name: filename
    in: path
    description: Name of a file located on the volume
    required: true
    schema:
      type: string
  
# API Calls for the /api/v1/volume
/volumes:
  post:
    operationId: addVolume
    summary: Add volume
    description: >
      Add volume. Note that the data in the cURL has to be adjusted based on the mountType. 
    tags: [ "Volumes" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/definitions/volumeByMountType'
    responses:
      201:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                id:
                    $ref: '#/definitions/Volume/properties/id'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL (CIFS)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes" --data '{"name":"Volume1","mountType":"cifs","mountOptions":{"host":"your.volume.host","remoteDir":"/remoteDir","username":"user","password":"yourPassword","seal":true}}'

      - lang: cURL (FILESYSTEM)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes" --data '{"name":"Volume1","mountType":"filesystem","mountOptions":{"hostPath":"your.volume.host.path"}}'

      - lang: cURL (MOUNTPOINT)
        source: |-
          curl -X POST \ 
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes" --data '{"name":"Volume1","mountType":"mountpoint","mountOptions":{"hostPath":"your.volume.host.path"}}'

      - lang: cURL (EXT4)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes" --data '{"name":"Volume1","mountType":"ext4","mountOptions":{"diskPath":"your.volume.disk.path"}}'
      
      - lang: cURL (XFS)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes" --data '{"name":"Volume1","mountType":"xfs","mountOptions":{"diskPath":"your.volume.disk.path"}}'

      - lang: cURL (SSHFS)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes" --data '{"name":"Volume1","mountType":"sshfs","mountOptions":{"host":"your.host.com", "remoteDir": "/backup", "port": 9876, "user": "Volume User", "privateKey": "SSH_PRIVATE_KEY"}}'
      
      - lang: cURL (NFS)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes" --data '{"name":"Volume1","mountType":"sshfs","mountOptions":{"host":"your.host.com", "remoteDir": "/backup"}}''
  get:
    operationId: getVolumes
    summary: List volumes
    description: List volumes
    tags: [ "Volumes" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                volumes:
                  type: array
                  items:
                    $ref: '#/definitions/Volume'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes"

/volumes/{id}:
  get:
    operationId: getVolume
    summary: Get volume
    description: Get volume
    tags: [ "Volumes" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/id'
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              $ref: '#/definitions/Volume'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes/$ID"
  post:
    operationId: updateVolume
    summary: Update volume
    description: Update volume. A volume can only be updated when it has the mount type `CIFS`, `NFS` or `SSHFS`. 
    tags: [ "Volumes" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/id'
    requestBody:
      required: true
      content:
        application/json:
          schema:
            oneOf:
              - $ref: '#/definitions/cifsMountOptions'
              - $ref: '#/definitions/nfsMountOptions'
              - $ref: '#/definitions/sshfsMountOptions'
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL (CIFS)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes/$ID" --data '{"name":"Volume1","mountType":"cifs","mountOptions":{"host":"your.volume.host","remoteDir":"/remoteDir","username":"user","password":"yourPassword","seal":true}}'

      - lang: cURL (SSHFS)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes/$ID" --data '{"name":"Volume1","mountType":"sshfs","mountOptions":{"host":"your.host.com", "remoteDir": "/backup", "port": 9876, "user": "Volume User", "privateKey": "SSH_PRIVATE_KEY"}}'
      
      - lang: cURL (NFS)
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes/$ID" --data '{"name":"Volume1","mountType":"sshfs","mountOptions":{"host":"your.host.com", "remoteDir": "/backup"}}''
  delete:
    operationId: deleteVolume
    summary: Delete volume
    description: Delete volume
    tags: [ "Volumes" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/id'
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X DELETE \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes/$ID"

/volumes/{id}/status:
  get:
    operationId: getVolumeStatus
    summary: Get volume status
    description: Get volume status
    tags: [ "Volumes" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/id'
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                state:
                  type: string
                  description: State of volume, active or not.
                  example: "active"
                message:
                  type: string
                  description: Last send status message.
                  example: "mounted"
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \ 
          -H "Content-Type: application/json" \ 
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes/$ID/status"

/volumes/{id}/remount:
  post:
    operationId: remountVolume
    summary: Remount volume
    description: Remount volume
    tags: [ "Volumes" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/id'
    responses:
      202:
        $ref: '../responses.yaml#/accepted'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \ 
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes/$ID/remount"

/volumes/{id}/files/{filename}:
  get:
    operationId: getFileFromVolume
    summary: Get a file from volume
    description: Get a file located on a volume.
    tags: [ "Volumes" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/id'
      - $ref: '#/parameters/filename'
    responses:
      200:
        description: Success
        content:
            text/plain:
              schema:
                type: string
              example: |
                This is the content of the file.
                It may contain multiple lines.
                Line 3...
                Line 4...
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \ 
          -H "Content-Type: application/json" \ 
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/volumes/$ID/files/$FILENAME"

