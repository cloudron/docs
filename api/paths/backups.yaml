definitions:
  backups:
    type: array
    items:
      type: object
      properties:
        id:
          type: string
          description: Unique identifier for the backup.
          example: box_v2.2.2_ff231fee
        remotePath:
          type: string
          description: Path where the backup is stored remotely.
          example: 2024-05-11-210000-872/box_v2.2.2
        label:
          type: string
          description: Label for the backup.
          example: backupLabel
        identifier:
          type: string
          description: Identifier for the type of backup.
          example: box
        creationTime:
          type: string
          description: Time when the backup was created.
          example: 22024-05-11T21:35:48.000Z"
        packageVersion:
          type: string
          description: Version of the backup package.
          example: "2.2.2"
        type:
          type: string
          description: Type of the backup.
          example: box
        state:
          type: string
          description: State of the backup.
          example: normal
        format:
          type: string
          description: Format of the backup file.
          example: tgz
        preserveSecs:
          type: number
          description: Number of seconds to preserve the backup.
          example: 0
        encryptionVersion:
          type:
            - "null"
            - number
          description: Version of the encryption used.
          example: 2
        dependsOn:
          type: array
          description: List of dependencies for the backup.
          items:
            type: string
            example: [app_3a09fbae-a1bc-4ec8-b3b9-6d72b3cf84d2_v1.1.0_9f3e294c, app_7b02debb-c1cd-4db6-a4c2-8e95a4ef02d1_v1.1.0_4b7d8a2e, app_e82bfcde-d2ef-4fdc-c8c1-5a94b5da06e3_v1.1.0_7c6f9b3d, app_1f93bdea-b3cd-4f2d-a9a5-2b83a5ec19f2_v1.1.0_2d9c8b4a, app_4d7aecfb-a3de-4cfb-b7b8-1c72b9fd93d4_v1.1.0_8a1b2c3d]
        manifest:
          type: object
          description: Manifest containing additional information about the backup.
          properties:
            name:
              type: string
              description: Name of the manifest.
              example: Manifest

  config:
    type: object
    description: Configuration settings for backups.
    properties:
      provider:
        type: string
        description: Backup provider.
        example: filesystem
      format:
        type: string
        description: Backup file format.
        example: tgz
      limits:
        type: object
        description: Limits for the backup process.
        properties:
          memoryLimit:
            type: number
            description: Memory limit for the backup process.
            example: 231214321
      backupFolder:
        type: string
        example: /var/backups
        description: Folder where backups are stored.
      noHardLinks:
        type: boolean
        example: false
        description: Indicator for using hard links.
      encryption:
        type:
          - "null"
          - string
        example: null
        description: Encryption method used for the backup.
  
  policy:
    type: object
    description: Policy settings for backups.
    properties:
      schedule:
        type: string
        example: "00 00 23 * * *"
        description: Backup schedule in cron format or similar.
      retention:
        type: object
        description: Retention policy for backups.
        properties:
          keepWWithinSecs:
            type: number
            description: Number of seconds to keep weekly backups.
            example: 172300

parameters:
  backupId:
    name: backupId
    in: path
    description: Specific ID of a backup.
    required: true
    schema:
      type: string

/backups:           
  get:
    operationId: listBackups
    summary: List backups
    description: List backups.
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '../parameters.yaml#/PaginationPage'
      - $ref: '../parameters.yaml#/PaginationPerPage'
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                backups:
                  type: array
                  items:
                    $ref: '#/definitions/backups/items'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups"

/backups/mount_status:
  get:
    operationId: getMountStatus
    summary: Get Mountstatus
    description: Get mount status of your backups.
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                state:
                  type: string
                  example: active
                message:
                  type: string
                  example: mounted
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/mount_status"

/backups/create:
  post:
    operationId: createBackups
    summary: Create backups
    description: Create backups.
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    responses:
      202:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                $ref: '../schemas.yaml#/task_info/properties'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/create
/backups/cleanup:     
  post:
    operationId: cleanUpBackups
    summary: Clean up backups
    description: Clean up backups.
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    responses:
      202:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                $ref: '../schemas.yaml#/task_info/properties'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/cleanup
/backups/remount:     
  post:
    operationId: remountBackups
    summary: Remount backups
    description: Remount backups
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    responses:
      202:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/remount
    
/backups/config:      
  get:
    operationId: getBackupConfig
    summary: Get config
    description: Get the configuration of your backups.
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              $ref: '#/definitions/config'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/config"
/backups/config/storage:
  post:
    operationId: setStorage
    summary: Set storage
    description: Set storage
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              provider:
                $ref: '#/definitions/config/properties/provider'
              format:
                $ref: '#/definitions/config/properties/format'
              password:
                type: string
                description: "The password used for encryption or authentication during backup. Please ensure to use a strong, secure password."
                example: Your_Password
              encryptedFilenames:  
                type: boolean   
                description: "Specifies whether filenames in the backup should be encrypted for added security."   
                example: true
            required:
              - password
              - encryptedFilenames
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/config/storage

/backups/config/limits:
  post:
    operationId: setLimits
    summary: Set limits
    description: Set limits
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                syncConcurrency:
                  type: integer
                  description: The maximum number of concurrent sync operations.
                copyConcurrency:
                  type: integer
                  description: The maximum number of concurrent copy operations.
                downloadConcurrency:
                  type: integer
                  description: The maximum number of concurrent download operations.
                deleteConcurrency:
                  type: integer
                  description: The maximum number of concurrent delete operations.
                uploadPartSize:
                  type: integer
                  description: The size of each part when uploading large files.
                memoryLimit:
                  type: integer
                  description: The memory limit for backup operations.
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/config/limits

/backups/policy: 
  get:
    operationId: getBackupPolicy
    summary: Get policy
    description: Get the policy of your backups.
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                policy:
                  $ref: '#/definitions/policy'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/policy"
  post:
    operationId: setPolicy
    summary: Set policy
    description: Set policy
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              schedule:
                $ref: '#/definitions/policy/properties/schedule'
              retention:
                $ref: '#/definitions/policy/properties/retention'
            required:
              - retention
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/policy

/backups/{backupId}:
  post:
    operationId: updateBackup
    summary: Update backup
    description: Update a backup by its ID.
    tags: [ "Backups" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/backupId'
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              label:
                $ref: '#/definitions/backups/items/properties/label'
              preserveSecs:
                $ref: '#/definitions/backups/items/properties/preserveSecs'
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/backups/$BACKUPID