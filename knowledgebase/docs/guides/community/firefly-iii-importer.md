# Running Firefly-III Importer

## Overview

This guide explains how to run the Firefly III Importer on a [Cloudron LAMP App](https://docs.cloudron.io/apps/lamp/).

!!! warning "Community Guide"
    This guide was contributed by the Cloudron Community, and does not imply support, warranty. You should utilize them at your own risk. You should be familiar with technical tasks, ensure you have backups, and throughly test.

* **Contributor**: [JLX89](https://forum.cloudron.io/user/jlx89)
* [Discuss this guide](https://forum.cloudron.io/category/48/firefly-iii)

## Getting Started

* Sign up with GoCardless API or Spectre. These services are free and can [import](https://docs.firefly-iii.org/data-importer/faq/spectre-and-nordigen/) most countries (except the United States).

* The importer does't seem to have an authentication gateway to prevent "public" access

* Your Personal Access Token is embedded in the `.env` file. 

### Installing the Data Importer

* Install a fresh LAMP App from the Cloudron Store. It's a good idea to [disable indexing](/apps/#robotstxt) via App > Security.

* Open the [file manager](/apps/#file-manager) and open the `PHP_VERSION file`. Change the value to PHP 8.2.
```
  ; Set the desired PHP version in this file
  ; Restart app for changes to take effect
  PHP_VERSION=8.2
```

* Save your file and restart the LAMP App.

* Launch the [terminal](/apps/#web-terminal) and complete the following commands:
```
  cd /app/data/
  composer create-project firefly-iii/data-importer
```

* Stay in the Terminal and next you'll want to make sure the permissions are correct by running the following commands:
```
  sudo chown -R www-data:www-data /app/data/data-importer
  sudo chmod -R 775 /app/data/data-importer/storage
```

* Next, you're going to need to update your Apache configuration to point from the default `/app/data/public/` folder to `/app/data/data-importer/public/`. Open `/app/data/apache/app.conf` and update the following:
  * Find the lines that have the previous public route of `/app/data/public` and change them to `/app/data/data-importer/public`. 
  * For good measure, I would disable phpMyAdmin also in the apache.conf file:
    `# Include "/app/code/apache/phpmyadmin.conf"`

* Restart the LAMP App

* **Personal Access Token**: You're going to need to generate a Personal Access Token from Firefly at: https://firefly.domain.tld/profile > OAUTH > Personal Token

* Launch the Importer's file manager and go to `/app/data/data-importer` . Open the `.env` file and update the file as required:
```
  FIREFLY_III_URL={https://firefly.domain.tld}
  VANITY_URL={https://firefly.domain.tld}
  FIREFLY_III_ACCESS_TOKEN={ACCESS-TOKEN}
  AUTO_IMPORT_SECRET={GENERATED_SECRET}
  TZ={America/New_York}
  ENABLE_MAIL_REPORT=false
  MAIL_DESTINATION={EMAIL-ADDRESS}
  MAIL_FROM_ADDRESS=getenv('CLOUDRON_MAIL_FROM')
  MAIL_HOST=getenv('CLOUDRON_MAIL_SMTP_SERVER')
  MAIL_PORT=getenv('CLOUDRON_MAIL_SMTP_PORT') 
  MAIL_USERNAME=getenv('CLOUDRON_MAIL_SMTP_USERNAME')
  MAIL_PASSWORD=getenv('CLOUDRON_MAIL_SMTP_PASSWORD')
  MAIL_ENCRYPTION=null
  REDIS_HOST=getenv('CLOUDRON_REDIS_HOST')
  REDIS_PASSWORD=etenv('CLOUDRON_REDIS_PASSWORD')
  REDIS_PORT=getenv('CLOUDRON_REDIS_PORT')
```

* Restart the LAMP App and you can access the interface at: https://firefly-importer.domain.tld

