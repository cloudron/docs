# Guide: Monitoring and Alerting for Cloudron Servers

## Introduction

As a Cloudron user, one of the challenges I've consistently faced is the lack of a built-in monitoring and alerting system. This guide aims to provide a straightforward solution for setting up monitoring on your Cloudron servers. Our goals are to make the setup process simple, enable notifications for alerts, handle multiple servers, and offer a freemium solution that starts free but scales with your needs.

!!! warning "Community Guide"
    This guide was contributed by the Cloudron Community, and does not imply support, warranty. You should utilize them at your own risk. You should be familiar with technical tasks, ensure you have backups, and throughly test.

* **Contributor**: [AmbroiseUnly](https://forum.cloudron.io/user/ambroiseunly)
* [Discuss this guide](https://forum.cloudron.io/topic/12015)

## Solution: Using Netdata

Netdata is a robust monitoring tool that meets all these requirements. It is simple to install and use, supports multiple servers, and provides immediate data visualization and alerting.

### Installation Steps

Follow these steps to install Netdata on your Cloudron server. The setup takes approximately 5 minutes:

1. **Create a Netdata Account**: Sign up for a free account on [Netdata](https://www.netdata.cloud/).
2. **Add Your First Node**: In your Netdata dashboard, navigate to `Deploy/Operating Systems/Linux` to add your first node.
3. **Copy the Command Line**: Netdata will provide a command line script (`wget...`) for installation.
4. **Execute the Command on Your Server**: Connect to your Cloudron server via SSH and run the provided command.
5. **Follow Installation Prompts**: Netdata will prompt you for confirmation to install as a background service.
6. **Verify Installation**: Check your Netdata dashboard to ensure data is being collected and displayed.

### Video Guide

For a detailed walkthrough, including a demonstration of the notification system, watch the following video guide:

[![Watch the video](https://forum.cloudron.io/assets/uploads/files/1719834436109-9ba8e13b-c7a9-4da4-94e4-65f31cb913e8-image-resized.png)](https://www.loom.com/share/7e59877aad0b4c31b8b5f058afdd8930)

## Alternative Solutions

### Grafana

Grafana is another powerful open-source monitoring tool. However, it has a more complex installation and configuration process, which might be challenging for users unfamiliar with such setups. Many users, including myself, have found it difficult to configure the notification system in Grafana.

### Custom Scripts

Some community members have suggested using custom bash scripts for monitoring. While this is a viable option, it requires significant maintenance and expertise. As someone who is not a sysadmin expert, I prefer relying on specialized tools like Netdata that offer ease of use and comprehensive features out of the box.

## Conclusion

Netdata provides an efficient and user-friendly solution for monitoring Cloudron servers. Its simplicity, combined with robust features, makes it an ideal choice for users looking to set up monitoring and alerting quickly and effectively.
