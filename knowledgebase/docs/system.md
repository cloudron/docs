# System

The System view gives an overview of the Server, CPU, Memory & Disk usage.

## Info

<center>
<img src="../img/system-info.png" class="shadow" width="500px">
</center>

## Logs

The `Logs` button can be used to view the System logs. In the situation that the dashboard is unreachable/down, the raw logs are located at `/home/yellowtent/platformdata/logs/box.log`. Up to 10MB of active logs is kept along side 5 rotated logs. Logs older than 14 days are removed.

<center>
<img src="../img/system-logs.png" class="shadow" width="500px">
</center>

## CPU Usage

<center>
<img src="../img/system-cpu.png" class="shadow" width="500px">
</center>

## Memory Usage

<center>
<img src="../img/system-memory.png" class="shadow" width="500px">
</center>

## Disk Usage

Disk usage is only computed on demand. Clicking on the `Refresh` button
in the top right will compute disk usage in that instant.

<center>
<img src="../img/system-disk-usage.png" class="shadow" width="500px">
</center>

| Directory | Notes |
|-----------|------ |
| `docker` | Size of docker images. Cloudron's docker images take around 10GB. In addition, app images vary greatly in size and it's normal for each app to be around 2GB in size.
| `docker-volumes` | Temporary data of apps. If this is large, find the offending app with `docker ps -q | xargs -I {} sh -c 'echo "Container: {}"; docker exec {} du -sh /run /tmp'` . |
| `/apps.swap` | System swap. Usually same amount as system RAM, but limited to 4GB max. |
| `boxdata` | Box (Cloudron code) data. |
| `maildata` | Email data. |
| `Everything else (Ubuntu, etc)` | System packages. Use system tools like `du` to investigate if this is more than say 20GB |
| `platformdata` - This contains logs, database directories (mysql, postgres, mongo, redis), logs and performance metrics. |
| App | Persistent data of app excluding database size |

!!! note "Excluding disks"
    If your disk is too slow, it can be excluded from periodic disk usage
    collection. Add the filesystem path (much match `df` output) per line
    to `/home/yellowtent/platformdata/diskusage/exclude`.

