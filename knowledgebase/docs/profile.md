# Profile

## Account settings

Users can view and edit their personal information in the `Profile view:

<center>
<img src="/img/profile-account.png" class="shadow" width="500px">
</center>

### Username

Username is used to login to the Dashboard and the apps. The username cannot be changed. To change the username,
the admin has to delete the old username and create a new one.

### Display name

The Display Name is the first name and last name of the user.

Display Name cannot be changed if the admin has [locked user profiles](/user-directory/#lock-profile) or if the user is from an [external directory](/user-directory/#external-directory).

### Primary email

The Primary email is the email that is exposed to apps. Apps may send notifications to the user to this email address.
This can be set to an email address hosted on Cloudron.

Primary email cannot be changed if the admin has [locked user profiles](/user-directory/#lock-profile) or if the user is from an [external directory](/user-directory/#external-directory).

### Password recovery email

The Password recovery email is the email to which Cloudron password resets are sent. This should be set to an email address that is not hosted on Cloudron.
If not set, it defaults to the [Primary email](#primary-email).

Password recovery email cannot be changed if the admin has [locked user profiles](/user-directory/#lock-profile) or if the user is from an [external directory](/user-directory/#external-directory).

## Icon

The profile icon or gravatar of a user can be changed by clicking on the profile icon.

<center>
<img src="/img/profile-avatar.png" class="shadow" width="500px">
</center>

## Background image

A background image for the dashboard can be set using the `Set Background Image` button:

<center>
<img src="/img/profile-background-image-button.png" class="shadow" width="500px">
</center>

When set, the dashboard will have a background:

<center>
<img src="/img/profile-background-image.png" class="shadow" width="500px">
</center>

## Enabling 2FA

2FA can be enabled using the `Enable 2FA button` from the `profile` view in the dashboard.

Clicking on the button will display a QR Code which can be scanned
using a TOTP app such as Google Authenticator ([Android](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2),
[iOS](https://itunes.apple.com/us/app/google-authenticator/id388497605)),
FreeOTP authenticator ([Android](https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp),
[iOS](https://itunes.apple.com/us/app/freeotp-authenticator/id872559395]))

<br/>

<center>
<img src="/img/profile-qrcode.png" width="500px" class="shadow">
</center>

2FA cannot be enabled if the user is from an [external directory](/user-directory/#external-directory) that supports 2FA e.g. when authenticating against another [Cloudron Directory Server](/user-directory/#cloudron).

## Disabling 2FA

Users can disable 2FA by clicking on the `Disable 2FA button`. In the event, the user
loses their 2FA device, a Cloudron administrator can [reset it](/user-management/#disable-2fa).

## App Passwords

App passwords can be used as a security measure in desktop, email & mobile clients. For example, if you
are trying out a new mobile app from an untrusted vendor, you can generate a temporary password that provides
access to a specific app.This way your main password does not get compromised (and thus providing access to other apps as well).

Click the 'New Password' button to create a new app password:

<center>
<img src="/img/profile-apppasswords-create.png" width="500px" class="shadow">
</center>

You can delete the password from the password list:

<center>
<img src="/img/profile-apppasswords-list.png" width="500px" class="shadow">
</center>

## API Tokens

[Cloudron API](/api.html) tokens can be created from the Profile view by clicking `New API Token`.

API Tokens are created with a readonly or read write scope:

<center>
<img src="/img/profile-api-tokens-create.png" width="500px" class="shadow">
</center>

Tokens can be viewed and revoked from the token listing:

<center>
<img src="/img/profile-api-tokens-list.png" width="500px" class="shadow">
</center>

## Language

Users can specify the Language setting for the Cloudron dashboard using the language selector:

<center>
<img src="/img/profile-language.png" width="500px" class="shadow">
</center>
