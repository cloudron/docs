# Unreachable Dashboard

Follow the instructions below if Cloudron dashboard is unreachable. If the dashboard is reachable, see the following
sections to troubleshoot specific issues:

* [App issues](/apps/#troubleshooting)
* [Mail Server issues](/email/#troubleshooting)
* [Service issues](/services/#troubleshooting)

## Troubleshooting Tool

If the Cloudron dashboard is unreachable, the first step is to SSH into the server and run the troubleshooting tool
- `cloudron-support --troubleshoot`.

The output of the tool should give an idea of what is not working. If the checks fail, the tool will give further
suggestions. Each of the tests are explained in the sections below.

```
# cloudron-support --troubleshoot
Vendor: Hetzner Product: vServer
Linux: 6.8.0-50-generic
Ubuntu: noble 24.04
Processor: AMD EPYC Processor
BIOS NotSpecified  CPU @ 2.0GHz x 3
RAM: 3911360KB
Disk: /dev/sda1        57G
[OK]	node version is correct
[OK]	IPv6 is enabled and public IPv6 address is working
[OK]	docker is running
[OK]	docker version is correct
[OK]	MySQL is running
[OK]	nginx is running
[OK]	dashboard cert is valid
[OK]	dashboard is reachable via loopback
[OK]	box v8.2.0 is running
[OK]	netplan is good
[OK]	DNS is resolving via systemd-resolved
[OK]	Dashboard is reachable via domain name
[OK]	Domain smartserver.io is valid and has not expired
[OK]	unbound is running
```

## Tests

### Node version

Cloudron is developed and tested against a specific node version. This might have changed if you installed nodejs on the server.
Cloudron does not support making changes to the base system. Follow the instructions provided in the output to revert the node version.

### IPv6

On some VPS providers, IPv6 is enabled and an address is assigned but IPv6 routing does not work. As a result, your server cannot
reach other IPv6 servers. The tool should provide instructions on disabling IPv6 as a temporary measure. You should contact
your VPS provider's support to fix the issue long term.

### Docker

When docker is not running, check `journalctl -u docker` and `systemctl status docker` for clues.
Docker sometimes obsoletes options between versions. Check if you have a custom config at `/etc/systemd/system/docker.service.d` that
requires fixing.

### Incorrect Docker Version

Cloudron is developed and tested again a specific docker version. This might have changed if you installed docker or compose on the server.
Cloudron does not support making changes to the base system. Follow the instructions provided in the output to revert the docker version.

### MySQL

!!! note "Two instances of MySQL"
    There are two instances of MySQL on Cloudron. One instance runs on the host and is used by the platform. Another instance is the
    MySQL addon which runs in a container named `mysql` and is shared by apps.  This test is related to the host MySQL.

Check the systemd MySQL service for clues using `systemctl status mysql` . Check for errors and warnings in `/var/log/mysql/error.log`.

Recovering from MySQL corruption is a complex technical topic. You can try to restore the quick method below failing which it's best to
[restore your server](/backups/#restore-cloudron) entirely from the latest Cloudron backup.

* Locate the latest box backup in your backup storage . This is a file named `box.tar.gz` (tgz) or `box` (rsync) . Extract or locate the file `box.mysqldump` inside it.
  Copy this file to some location in your server.
* Stop box code: `systemctl stop box` .
* Stop MySQL - `systemctl stop mysql`
* Recreate the existing MySQL installation:

```
# mv /var/lib/mysql /var/lib/mysql.old
# mkdir /var/lib/mysql
# chown -R mysql:mysql /var/lib/mysql
# mysqld --initialize   # This will dump the MySQL root password in /var/log/mysql/error.log
```

* Start MySQL - `systemctl start mysql`

* Change the root password to `password` (sic):

```
# mysql -uroot -p<password from /var/log/mysql/error.log>  # there is no space between p and the password. e.g -pAS23kdI
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 6365
Server version: 5.7.25-0ubuntu0.18.04.2 (Ubuntu)

mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'password';
```

* Import the database - `mysql -uroot -ppassword < /path/to/box.mysqldump`

* Start the platform code again - `systemctl restart box`

### nginx

nginx is the reverse proxy that serves up the Cloudron dashboard and the apps. Use `journalctl -u nginx` and check `/var/log/nginx/error.log`
to investigate why it is not running.

Often, the issue is that some config is incorrect or corrupt.  nginx configs are located at `/home/yellowtent/platformdata/nginx/` . Delete some files here and try to make `systemctl restart nginx` work. You can safely delete nginx certificates and config files and attempt to get it running. Cloudron can regenerate certificates and configs from it's database. Once you have access to the dashboard, go to App -> Repair to regenerate the nginx config.

### Dashboard Certificate

Invalid certificates are often caused by invalid DNS API tokens (wildcard certs) or incoming port 80 to server is blocked by the firewall
([manual](/domains/#manual-dns) or [wildcard DNS](/domains/#wildcard-dns) provider).

To fix, visit the dashboard and accept the self-signed certificate in the browser. Then, go to Domains view and update the DNS API Token.
Then, renew the certs using `Domains` -> `Renew Certs`.

!!! note "API Token may be tied to IP"
    Some DNS API tokens (cloudflare, namecheap) can be restricted by IP. If your server's IP changed, then the token rules have to be fixed.

### Loopback / Hairpin NAT

Cloudron configures the DNS to be the public IP address. If the router does not support [Hairpin NAT](https://en.wikipedia.org/wiki/Network_address_translation#NAT_loopback), then you will have trouble reaching the dashboard and the apps from inside your network.

There are many ways to fix this and the solution depends on your use case. Here are some suggestions:

* If you intend to use Cloudron and the apps from only inside your network, change the [network settings](/networking/#ipv4) to use the local IP address. When doing so, Cloudron will use the local IP in the DNS instead of the public IP address.

* If you intend to use Cloudron from outside your network as well, the easiest fix is to purchase a new router that supports hairpin NAT.

* If purchasing a new router is not an option:

    * Configure your network's DNS server to return the Local VM IP for all the subdomain in use. This way when your PC/Laptop accesses a domain, it starts using the Local VM IP instead of the public IP to connect to Cloudron. Devices outside the network will continue to use the public IP address as expected.

    * Some apps use the domain name to connect with each other (For example, collabora app connects to nextcloud app with domain name). For such cases, configure Cloudron's DNS server `unbound` to use your [network's DNS](/networking/#internal-dns-server).

### Netplan

Ubuntu uses netplan by default to configure networking. This is an optional component and your VPS provider could have disabled it.
When enabled, the configs are usually located at `/etc/netplan/50-cloud-init.yaml`. This file contains information on how your server
learns about it's IP and nameservers. netplan generates systemd (default) or NetworkManager configs (desktop Ubuntu) based on `network.renderer`.

### DNS Resolution

`systemd-resolved` is the Ubuntu system DNS. It runs on `127.0.0.53`.

Use `resolvectl` to see the name servers being used. Usually, the system gets it's nameservers via [netplan configs](#netplan). You can
also hardcode the nameservers in `/etc/systemd/resolved.conf`. Set `DNS=1.1.1.1`, for example.

* Check if `systemctl status systemd-resolved` is running.
* Check if `host www.cloudron.io 127.0.0.53` works.
* Check if `systemd-resolved` is the systemd DNS by checking if `/etc/resolv.conf` has `nameserver 127.0.0.53`

### Domain issues & expiry

First, check if the domain points to your server.

Next, check if your domain has not expired. If the domain has expired and you renew it, you have to wait a bit for the DNS records to come back. This is because
most registrars replace your DNS with parking pages until you renew. Some registrars, won't revert
back your old DNS records. In such situations, add a DNS A record for `my.example.com` to point to the server's IP. Then,
wait and a bit for the DNS to propagate and login to the dashboard. Go to `Domains` view and click the [Sync DNS](/domains/#sync-dns)
button to re-add Cloudron related DNS records.

If the domain has expired, but you don't plan to renew it, then edit your PC/Mac's `/etc/hosts` and add an entry (the old domain and not the new one) to point to the server.

```
1.2.3.4 my.example.com
```

* With the above entry in place, you can visit `https://my.example.com` in your browser. You may have to purge the site from your browser's history to get over HSTS certificate issues.

* Login and go to `Domains` view. Add the new domain.

* [Change the dashboard domain](https://docs.cloudron.io/domains/#dashboard-domain) to this new domain.

* Move all the apps one by one from the old domain to the new domain by [changing the location](/apps/#location).

* Delete the old domain and remove the entry we added earlier from `/etc/hosts`.

## Failed upgrade

If the domain looks ok and nginx is running fine, check the box logs at `/home/yellowtent/platformdata/logs/box.log`.

To make sure, all migrations are up to date:

* Run `/home/yellowtent/box/setup/start.sh` on the server.
* Check `systemctl status box`. If it's not running, try `systemctl restart box`

If it still has errors, please contact [support](/support).
