# Services

## Overview

The `Services` view can be used to monitor the status and manage the configuration
of services used by apps. Services are managed entirely by Cloudron. When apps are deployed,
Cloudron provisions and configures services internally.

You don't have to worry about keeping the services updated - it's part of the Cloudron platform.

<center>
<img src="../img/services.png" class="shadow" width="500px">
</center>

## Configure

To configure a service, use the `Configure` button:

<center>
<img src="../img/services-configure.png" class="shadow" width="500px">
</center>


This will open up a dialog with various settings depending on the service:

<center>
<img src="../img/services-configure-dialog.png" class="shadow" width="500px">
</center>

## Logs

To view the logs of a services, click the `Logs` button:

<center>
<img src="../img/services-logs.png" class="shadow" width="500px">
</center>

Up to 10MB of logs is retained per service along side 1 rotated log. Logs older than 14 days are removed.
The raw logs are located under `/home/yellowtent/platformdata/logs/<servicename>`.


## Troubleshooting

If a service is not running, the first step is to restart it and/or to try increasing the memory limit.

<center>
<img src="/img/system-memorylimit.png" class="shadow" width="500px">
</center>

Check the service logs for hints of why it's not starting up.

### Corrupt Addon

The instructions here apply to the following database addons:

| Database | Container name | Logs | Data Directory |
| -------- | -------------- | ---- | -------------- |
| MySQL    | mysql          | /home/yellowtent/platformdata/logs/mysql/app.log | /home/yellowtent/platformdata/mysql |
| PostgreSQL    | postgresql | /home/yellowtent/platformdata/logs/postgresql/app.log | /home/yellowtent/platformdata/postgresql |
| MongoDB | mongodb | /home/yellowtent/platformdata/logs/mongodb/app.log | /home/yellowtent/platformdata/mongodb |
| Redis    | redis-<appid>          | /home/yellowtent/platformdata/logs/redis-<appid>/app.log | /home/yellowtent/platformdata/redis/<appid> |

In case of disk corruption, it's best to just start afresh and restore the data from backups.

To restore a specific database:

```
docker stop <containername>
mv /home/yellowtent/platformdata/<database> /home/yellowtent/platformdata/<database>-copy # stash the database data directory. see table above
mkdir /home/yellowtent/platformdata/<database> # create a fresh database directory
docker restart <containername> # start the addon. this will re-initialize the addon with no data
```

Now, restore each app that uses the addon in the Cloudron dashboard.
