# <img src="/img/documenso-logo.png" width="25px"> Documenso App

## About

The Open Source DocuSign Alternative.

* Questions? Ask in the [Cloudron Forum - Documenso](https://forum.cloudron.io/category/192/documenso)
* [Documenso Website](https://documenso.com/)
* [Documenso issue tracker](https://github.com/RocketChat/Rocket.Chat/issues)

## Signing Certificate

For the digital signature of your documents, you need a signing certificate in .p12 format (public and private key).

You can either upload an existing one or generate a new one.

### Existing Certificate

If you already have a signing certificate, upload it as `/app/data/resources/cert.p12` using the [File Manager](/apps/#file-manager).

If the certificate has a passphrase, set it in `/app/data/env` file:

```
NEXT_PRIVATE_SIGNING_PASSPHRASE={YourStrongPassHere}
```

Restart the app for the changes to take effect.

### Generate Certificate

To create a self-signed certificate, open a [web terminal](/apps#web-terminal) and run the following commands:

```
$ cd /run

# generate private key
$ openssl genrsa -out private.key 2048

# generate self-signed certificate valid for 10 years
$ openssl req -new -x509 -key private.key -out certificate.crt -days 3650

# create p12 certificate. password is optional
$ openssl pkcs12 -export -out cert.p12 -inkey private.key -in certificate.crt -legacy

# move cert into documenso
$ mv /run/cert.p12 /app/data/resources/cert.p12
$ chown cloudron:cloudron /app/data/resources/cert.p12
```

If you set an optional password, open [File Manager](/apps/#file-manager) and put  passphrase into `/app/data/env` file:

```
NEXT_PRIVATE_SIGNING_PASSPHRASE={YourStrongPassHere}
```

Restart the app for changes to take effect.
