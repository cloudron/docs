# <img src="/img/outline-logo.png" width="25px"> Outline App

## About

Outline is the fastest knowledge base for growing teams.

* Questions? Ask in the [Cloudron Forum - Outline](https://forum.cloudron.io/category/178/outline)
* [Outline Website](https://www.getoutline.com/)
* [Outline Forum](https://github.com/outline/outline/discussions/)

## Login

### Without Cloudron Directory

Outline requires at least one SSO provider configured. This means that when authentication against Cloudron Directory is disabled,
there is no way to login to the app and you will simply see a login screen with no input fields post installation.

To proceed:

* Configure [one of the](https://github.com/outline/outline/blob/b08a430131a4e564694cf43bc28f287efb1b9ce5/.env.sample#L69) SSO methods in `/app/data/env.sh` using the [File manager](/apps/#file-manager).
* Restart the app

## License

Please be aware of the BSL [license restrictions](https://github.com/outline/outline/blob/main/LICENSE) before installing Outline – selling, reselling, or hosting Outline as a service is a breach of the terms and automatically terminates your rights under the license.

See https://docs.getoutline.com/s/hosting/ for more information.

## Custom config

[Custom environment variables](https://docs.getoutline.com/s/hosting/) can be set in `/app/data/env.sh` using the [File manager](/apps/#file-manager). For example:

```
export FILE_STORAGE_UPLOAD_MAX_SIZE=26214400
export MAXIMUM_IMPORT_SIZE=5120000
```

Be sure to restart the app after making any changes.

