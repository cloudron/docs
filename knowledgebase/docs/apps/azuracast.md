# <img src="/img/azuracast-logo.png" width="25px"> AzuraCast App

## About

AzuraCast is a free and open-source, self-hosted web radio station in a box.

* Questions? Ask in the [Cloudron Forum - AzuraCast](https://forum.cloudron.io/category/191/azuracast)
* [AzuraCast Website](https://www.azuracast.com/)
* [AzuraCast issue tracker](https://github.com/AzuraCast/AzuraCast/issues)
