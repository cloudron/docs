# <img src="/img/formbricks-logo.png" width="25px"> Formbricks App

## About

A privacy-first Experience Management suite built on the largest open source survey platform worldwide. Gather feedback on websites, apps and everywhere else to understand what your customers need.

* Questions? Ask in the [Cloudron Forum - Formbricks](https://forum.cloudron.io/category/198/formbricks)
* [Formbricks docs](https://formbricks.com/docs/introduction/what-is-formbricks)
* [Formbricks issue tracker](https://github.com/formbricks/formbricks/issues)

## Default user role

You can update a default user role set for users on first login by setting `DEFAULT_ORGANIZATION_ROLE` in `/app/data/env`.
