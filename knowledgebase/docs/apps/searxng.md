# <img src="/img/searxng-logo.png" width="25px"> SearXNG App

## About

SearXNG is a privacy-respecting metasearch engine.

* Questions? Ask in the [Cloudron Forum - SearXNG](https://forum.cloudron.io/category/47/searxng)
* [SearXNG Website](https://docs.searxng.org/)
* [SearXNG issue tracker](https://github.com/searxng/searxng/issues)

## Custom configuration

Use the [File Manager](/apps#file-manager) to edit `/app/data/settings.yml`. See [this file](https://github.com/searxng/searxng/blob/master/searx/settings.yml)
for the possible options.

