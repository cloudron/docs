# <img src="/img/openproject-logo.png" width="25px"> OpenProject App

## About

OpenProject is an efficient classic, agile or hybrid project management in a secure environment.

* Questions? Ask in the [Cloudron Forum - OpenProject](https://forum.cloudron.io/category/31/openproject)
* [OpenProject Website](https://www.openproject.org/)
* [OpenProject forum](https://community.openproject.com/projects/openproject/boards/)
* [OpenProject issue tracker](https://community.openproject.com/projects/openproject/boards/2905)

## User management

### Cloudron Directory

Cloudron users can login to the OpenProject. The Cloudron *admin* status however is not carried over to the app, thus it comes with a pre-setup admin account,
which requires a password change upon first login. Check the post-install notes when installing the app for admin account username and password.

OpenProject supports various authentication methods in parallel, this means even when Cloudron Directory is used,
non Cloudron users can still be invited to the app.

### Without Cloudron Directory

The app has a pre-setup admin account, which requires a password change upon first login.
Check the post-install notes when installing the app for admin account username and password.
Other users can be invited or added by this admin.

## Custom Configuration

Use the [File Manager](/apps#file-manager) to edit custom configuration in `/app/data/env.sh`.
See the [OpenProject docs](https://www.openproject.org/docs/installation-and-operations/configuration/environment/#supported-environment-variables) for all available options.

Once the file was changed, the app has to be restarted to apply the changes.
