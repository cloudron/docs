# <img src="/img/wbo-logo.png" width="25px"> WBO App

## About

WBO is an online collaborative whiteboard that allows many users to draw simultaneously on a large virtual board.

* Questions? Ask in the [Cloudron Forum - WBO](https://forum.cloudron.io/category/116/wbo)
* [WBO Website](https://wbo.ophir.dev/)
* [WBO issue tracker](https://github.com/lovasoa/whitebophir/issues)

## Custom config

[Custom config](https://github.com/lovasoa/whitebophir/blob/master/server/configuration.js) can be set in
`/app/data/env` using the [File manager](/apps/#file-manager). Be sure to reboot the app after making changes.

