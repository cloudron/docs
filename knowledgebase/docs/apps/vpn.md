# <img src="/img/vpn-logo.png" width="25px"> VPN App

## About

VPN app allows you to create a trusted and secure private network. It supports both OpenVPN and WireGuard.

* Questions? Ask in the [Cloudron Forum - OpenVPN](https://forum.cloudron.io/category/20/openvpn)
* [OpenVPN Website](https://openvpn.org/)
* [WireGuard Website](https://www.wireguard.com/)
* [Upstream OpenVPN forum](https://community.openvpn.net/openvpn)

## Desktop and Mobile Clients

The OpenVPN app has been tested with the following clients:

* NetworkManager on Ubuntu
* [Tunnelblick](https://www.tunnelblick.net/) on Mac OS X
* [OpenVPN for Android](https://play.google.com/store/apps/details?id=de.blinkt.openvpn)
* [WireGuard for Android](https://play.google.com/store/apps/details?id=com.wireguard.android&hl=en_US)
* [OpenVPN for Mac](https://openvpn.net/client-connect-vpn-for-mac-os/)

## Configs

The OpenVPN config can be downloaded using the download button. There are a couple of formats:

* `OpenVPN` - For linux and OpenVPN app on Android
* `Tunnelblick` - For Tunnelblick
* `WireGuard` - For WireGuard

<center>
<img src="/img/vpn-config.png" class="shadow" width="500px">
</center>

## How to connect on Ubuntu

Download the .ovpn embedded certs config file from the OpenVPN app. Then, in the Network settings in Ubuntu,
click on 'Add VPN' and then `Import from file...`.

<center>
<img src="/img/vpn-ubuntu.png" class="shadow" width="500px">
</center>

## Admin Settings

The first user to login is made the admin.

The admin panel can be used to customize some of the popular settings like
the network address, DNS server and client-to-client connectivity.

To make another user an admin, edit the file `/app/data/vpn.db` and edit the
`admins` field. This is a JSON database, so be careful with quotations.

<center>
<img src="/img/vpn-settings.png" class="shadow" width="500px">
</center>

## Built-in DNS Server

This app has a built-in Dnsmasq DNS server which is pushed to clients. This DNS server
allows resolution of connected clients using `devicename.username`.

You can configure this DNS server by editing `/app/data/dnsmasq.conf` using the [File manager](/apps#file-manager).
For example, to make Dnsmasq forward DNS requests to an internal DNS server, use the following:

```
server=internal-server-ip
```

See [Dnsmasq docs](http://thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html) for all the available options.

## Custom DNS Server

You can configure the VPN to use a custom DNS server - for example, an external AdGuard or a Pi-Hole installation.
Simply, put in the IP of the DNS server in the [Admin Settings](/#admin-settings). In the screenshow below,
`104.207.150.252` is the AdGuard DNS Server IP.

<center>
<img src="/img/vpn-adguard.png" class="shadow" width="500px">
</center>

## Privacy

The VPN app provides a tunnel to channel all the traffic from your devices via the Cloudron. Websites and services that you visit will
not see the IP address of your devices but they *will* see the IP address and possibly the RDNS hostname of your Cloudron.

You can check what sort of information can be gathered from your Cloudron's IP address using [ipleak.net](https://ipleak.net).

## IPv6

### Transport

If you have enabled [IPv6 support](/networking/#ipv6) on Cloudron, Clients can connect (the "transport" protocol) to the OpenVPN server via IPv6. Whether the connection uses IPv4 or IPv6 is a client OS preference.

### Inside Tunnel

IPv6 is supported inside the tunnel (independent of the transport protocol). The server needs to have an IPv6 address assigned for IPv6 tunneling to work.

## OpenVPN

### TCP/UDP

OpenVPN can be configured to use TCP or UDP. TCP mode uses more bandwidth and slower than UDP.

The UDP port or TCP port can be configure the app's [Location](/apps/#location) section. If
UDP is enabled, TCP is automatically disabled.

<center>
<img src="/img/vpn-tcp-udp.png" class="shadow" width="500px">
</center>

### OpenVPN Customization

You can customize various settings by editing `/app/data/openvpn.conf` using
the  [File Manager](/apps#file-manager). Some popular options are
discussed below:

### Custom OpenVPN routes

By default, clients are configured to route all traffic via the VPN. If you disable this, you would
want to push custom routes for the network and hosts behind the VPN. For example, edit the file as below and
restart the app.

```
# push "redirect-gateway def1 bypass-dhcp"
push "route 178.128.183.220 255.255.255.255"
push "route 178.128.74.0 255.255.255.0"
```

### Custom Client Configuration

Custom Client Configuration allows the OpenVPN admin to assign a specific IP address to a client or push specific options 
such as compression and DNS server to a client.

To add custom settings:

* Edit `/app/data/openvpn.conf` and add the following line:
```
client-config-dir /app/data/ccd
```

* Create the directory `/app/data/ccd`

* You can create custom client configs in this directory by creating files with the name `[username]_[devicename]`. You can also
  create a file named `DEFAULT` which will be used if no device specific file exists.

* For example, to assign a static IP to a client, you can add the line `ifconfig-push 10.8.0.50 10.8.0.51` (requires IP pair)

* Restart the app for changes to take effect.


### Concurrent Connections

By default, every connection must have one unique cert. With `duplicate-cn`, one cert can be used by more than one connection.

Enable this if multiple clients with the same VPN config can concurrently connect:

```
duplicate-cn
```

## Troubleshooting

If you are unable to connect to the OpenVPN server, make sure that your VPS firewall allows the OpenVPN/WireGuard ports. For example, you might have
to add this incoming port as part of EC2 security group.

