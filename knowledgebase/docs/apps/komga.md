# <img src="/img/komga-logo.png" width="25px"> Komga App

## About

Komga is a media server for your comics, mangas, BDs, magazines and eBooks.

* Questions? Ask in the [Cloudron Forum - Umami](https://forum.cloudron.io/category/200/komga)
* [Komga Website](https://komga.org/)
* [Komga docs](https://komga.org/docs/introduction)
* [Komga issue tracker](https://github.com/gotson/komga/issues)

## Reset password

To [reset password](https://komga.org/docs/guides/cli/#reset-password-for-a-user) of a user:

* Place the app in [recovery mode](/apps/#recovery-mode)
* In the [Web Terminal](/apps/#web-terminal), run the following:

    ```
    /usr/local/bin/gosu cloudron:cloudron java -jar komga.jar --komga.config-dir=/app/data/komga --reset=test@cloudron.io --newpassword=YourNewPassword 
    ```

* You should already be able to login to Komga with the new password

* Disable Recovery Mode

