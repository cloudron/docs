# <img src="/img/rss-bridge-logo.png" width="25px"> RSS-Bridge App

## About

RSS-Bridge is a PHP project capable of generating RSS and Atom feeds for websites that don't have one.

* Questions? Ask in the [Cloudron Forum - RSS-Bridge](https://forum.cloudron.io/category/131/rss-bridge)
* [RSS-Bridge Wiki](https://github.com/RSS-Bridge/rss-bridge/wiki)
* [RSS-Bridge issue tracker](https://github.com/RSS-Bridge/rss-bridge/issues)

## Whitelist

RSS-Bridge supports [whitelists](https://github.com/RSS-Bridge/rss-bridge/wiki/Whitelisting) in order to limit
the available bridges on your web server. The Cloudron package whitelists all the available bridges, by default.
To change this, edit `/app/data/whitelist.txt` using the [File Manager](/apps/#file-manager).

