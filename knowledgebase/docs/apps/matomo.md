# <img src="/img/piwik-logo.png" width="25px"> Matomo App

## About

Matomo is Google Analytics alternative that protects your data and your customers' privacy

* Questions? Ask in the [Cloudron Forum - Matomo](https://forum.cloudron.io/category/87/matomo)
* [Matomo Website](https://matomo.org/)
* [Matomo forum](https://forum.matomo.org/)
* [Matomo issue tracker](https://github.com/matomo-org/matomo/issues/)

## Installing plugins

Matomo plugins can be installed from the Marketplace view. Only the Matomo super-user
can install plugins (Matomo admins cannot install plugins).

Plugins can also be installed by manually extracting them into `/app/data/plugins` using
the [File manager](/apps/#file-manager).

## Debugging the tracker

To [debug the tracker](https://developer.matomo.org/api-reference/tracking-api#debugging-the-tracker), add the following to `config.ini.php`
using the [file manager](/apps/#file-manager):

```

[Tracker]
debug = 1
debug_on_demand = 1
```

You can then debug the request using the following curl request (be sure to fix up the idsite):

```
curl -X POST 'https://matomo.example.com/matomo.php?idsite=1&rec=1&bots=1&debug=1
```

## System check warning

### LOAD DATA INFILE

The LOAD DATA INFILE warning can be ignored. More information is available in matomo forum - [here](https://forum.matomo.org/t/trying-to-move-out-load-data-infile/39889)
and [here](https://forum.matomo.org/t/can-i-ignore-the-load-data-infile-error-in-adminstration-system-check/28210). There is no loss of functionality by ignoring the warning.

From Cloudron point of view, the LOAD DATA INFILE feature requires MySQL and Matomo to share the same file system. This is a security risk and not desirable. 
On Cloudron, apps and database containers are isolated and cannot access each other's file system.

