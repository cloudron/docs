# <img src="/img/paperless-ngx-logo.png" width="25px"> Paperless-ngx App

## About

Paperless-ngx is an application that manages your personal documents. With the help of a document scanner (see Scanner recommendations),
paperless transforms your wieldy physical document binders into a searchable archive and provides many utilities for finding and managing your documents.

* Questions? Ask in the [Cloudron Forum - Paperless-ngx](https://forum.cloudron.io/category/137/paperless-ngx)
* [Paperless-ngx website](https://docs.paperless-ngx.com)
* [Paperless-ngx community](https://github.com/paperless-ngx/paperless-ngx)
* [Paperless-ngx issue tracker](https://github.com/paperless-ngx/paperless-ngx/issues/)

## Custom config

Custom configuration can be set in `/app/data/paperless.conf` using the [File manager](/apps/#file-manager).
See [upstream docs](https://docs.paperless-ngx.com/configuration/) for various
options.

## Uploading

Files should be uploaded to `/app/data/consume` using the [File manager](/apps/#file-manager). Once uploaded,
a background task scans the documents automatically.

## Document retagger

To run the [document retagger](https://docs.paperless-ngx.com/administration/#retagger), open a [Web Terminal](/apps/#web-terminal) and execute:

```
# cd /app/code/src
# python3 manage.py document_retagger -T
```

## Exporting

To export existing documents, open a [Web Terminal](/apps/#web-terminal) and execute:

```
# cd /app/code/src
# mkdir -p /app/data/out
# python3 manage.py document_exporter /app/data/out
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████| 1/1 [00:00<00:00, 545.92it/s]
```

## Importing

To import an existing export at `/app/data/in`, open a [Web Terminal](/apps/#web-terminal) and execute:

```
# cd /app/code/src
# python3 manage.py document_importer /app/data/in
Installed 4 object(s) from 1 fixture(s)
Copy files into paperless...
100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████| 1/1 [00:00<00:00, 34.11it/s]
Updating search index...
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████| 1/1 [00:00<00:00, 104.00it/s]
```

