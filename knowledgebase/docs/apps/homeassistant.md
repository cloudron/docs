# <img src="/img/homeassistant-logo.png" width="25px"> Home Assistant App

## About

Open source home automation that puts local control and privacy first. Powered by a worldwide community of tinkerers and DIY enthusiasts.

* Questions? Ask in the [Cloudron Forum - Home Assistant](https://forum.cloudron.io/category/196/home-assistant)
* [Home Assistant Help](https://www.home-assistant.io/help/)
* [Home Assistant Website](https://www.home-assistant.io/)
