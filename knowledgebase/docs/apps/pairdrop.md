# <img src="/img/pairdrop-logo.png" width="25px"> PairDrop App

## About

Local file sharing in your browser. Inspired by Apple's AirDrop. Fork of Snapdrop. 

* Questions? Ask in the [Cloudron Forum - PairDrop](https://forum.cloudron.io/category/193/pairdrop)
* [Pairdrop Website](https://github.com/schlagmichdoch/PairDrop)
* [PairDrop Issue Tracker](https://github.com/schlagmichdoch/PairDrop/issues)
* [PairDrop Discussions](https://github.com/schlagmichdoch/PairDrop/discussions)

## Custom RTC Config

To use this app with a custom TURN server:

* Create a file named `/app/data/rtc_config.json` using the [File Manager](/apps/#file-manager). See 
  [upstream example](https://github.com/schlagmichdoch/PairDrop/blob/master/rtc_config_example.json).

* Add the line below to `/app/data/env.sh` using the [File Manager](/apps/#file-manager).

```
export RTC_CONFIG=/app/data/rtc_config.json
```

* Disable Cloudron's TURN Server auto-configuration for the app in the [TURN section](/apps/#turn). Note that
  if you do not provide a custom config file above, the TURN server defaults to Google's server (stun.l.google.com).

