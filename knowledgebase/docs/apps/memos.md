# <img src="/img/memos-logo.png" width="25px"> Memos App

## About

Open Source, Self-hosted, Your Notes, Your Way. Effortlessly craft your impactful content.


* Questions? Ask in the [Cloudron Forum - Memos](https://forum.cloudron.io/category/197/memos)
* [Memos Website](https://www.usememos.com/)
* [Memos issue tracker](https://github.com/usememos/memos/issues)
