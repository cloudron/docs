# <img src="/img/superset-logo.png" width="25px"> Superset App

## About

Apache Superset is a modern data exploration and visualization platform.

* Questions? Ask in the [Cloudron Forum - Superset](https://forum.cloudron.io/category/159/superset)
* [Superset Website](https://superset.apache.org/)
* [Superset issue tracker](https://github.com/apache/superset/issues)

## Demo data

Demo data is a great way to get to know how superset works. To import open the [Web terminal](/apps#web-terminal) into the app and run the following commands:

```sh
source /app/pkg/env.sh
superset fab create-admin --username admin --firstname Superset --lastname Admin --email admin@cloudron.local --password changeme
superset load_examples
```

!!! note "Time and memory consuming"
    Demo data takes many minutes and a lot of memory to get fetched and imported. For this set the memory limit of the app at least to 2Gb

## Custom Config

[Custom configuration](https://superset.apache.org/docs/installation/configuring-superset) can be placed in `/app/data/config/config_user.py`.
For example:

```
FEATURE_FLAGS = {
  'SSH_TUNNELING': True
}
```

Be sure to restart the app after making any changes.

## Sqlite

To enable Sqlite connections, add the following to `/app/data/config/config_user.py` using the
[File manager](/apps/#file-manager). Be sure to restart the app after making any changes.

```
PREVENT_UNSAFE_DB_CONNECTIONS = False
```

Be sure to restart the app after making any changes.

!!! note "Sqlite SQLAlchemy path"
    SQLAlchemy's Sqlite path has 4 slashes for absolute paths. For example, `file:////app/data/sample/northwind.db`

## RBAC

To configure which roles can access a dashboard, [enable `DASHBOARD_RBAC`](https://superset.apache.org/docs/using-superset/creating-your-first-dashboard/#manage-access-to-dashboards) feature flag in `/app/data/config/config_user.py`.

```
FEATURE_FLAGS = {
    'DASHBOARD_RBAC': True
}
```

## Public Dashboards

For publicly accessible dashboards, try a config like below. Please read the [Superset docs](https://superset.apache.org/docs/security/#public) to understand the security implications.

```
AUTH_ROLE_PUBLIC = 'Public'
PUBLIC_ROLE_LIKE = 'Gamma'

FEATURE_FLAGS = {
    'DASHBOARD_RBAC': True
}
```

## User Role

The default role for a new user is administrator.

To change this, edit `/app/data/config/config_user.py` using the [File manager](/apps/#file-manager) and change `AUTH_USER_REGISTRATION_ROLE`. For example:

```
AUTH_USER_REGISTRATION_ROLE = "Public"
```

Be sure to restart the app after making any changes.

