# <img src="/img/invidious-logo.png" width="25px"> Invidious App

## About

Invidious is an open source alternative front-end to YouTube.

* Questions? Ask in the [Cloudron Forum - Invidious](https://forum.cloudron.io/category/179/invidious)
* [Invidious Website](https://invidious.io/)
* [Invidious Documentation](https://docs.invidious.io)
* [Invidious Contact](https://invidious.io/contact/)
* [Invidious Issue Tracker](https://github.com/iv-org/invidious/issues)

## Periodic Restart

The [upstream docs](https://docs.invidious.io/installation/#post-install-configuration) recommends
restarting Invidious often, at least once a day, ideally every hour because of various issues.

If you hit issues, you can set up a [cron job](/apps/#cron) to restart it periodically:

```
# restart every hour
0 * * * * echo "==> Restarting invidious periodically" && supervisorctl restart invidious
```

