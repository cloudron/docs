# <img src="/img/wallog-logo.png" width="25px"> Wallos App

## About

Wallos is an Open-Source Personal Subscription Tracker.

* Questions? Ask in the [Cloudron Forum - Wallos](https://forum.cloudron.io/category/201/wallos)
* [Wallos Website](https://github.com/ellite/Wallos)
* [Wallos issue tracker](https://github.com/ellite/Wallos/issues)

