# <img src="/img/kutt-logo.png" width="25px"> Kutt App

## About

Kutt is a free Modern URL Shortener.

* Questions? Ask in the [Cloudron Forum - Kutt](https://forum.cloudron.io/category/111/kutt)
* [Kutt Website](https://kutt.it)

## Custom config

Custom configuration can be put in `/app/data/env` using the [Web Terminal](/apps#web-terminal) or the
[File Manager](/apps/#file-manager). The `env` file contains various customization options with documentation on them.
After changing the file, make sure to restart the app.

## Themes

Themes can be placed in `/app/data/custom` directory using the [File Manager](/apps/#file-manager). See upstream
[docs](https://github.com/thedevs-network/kutt?tab=readme-ov-file#themes-and-customizations) on how to create
styles and change images.

## Registration

Registration is disabled by default. It can be enabled by editing `/app/data/env` using the [File manager](/apps/#file-manager):

```
DISALLOW_REGISTRATION=false
```

Be sure to restart the app after making changes.

## 3rd party packages

Kutt integrates with a variety of languages and frameworks. See [upstream docs](https://github.com/thedevs-network/kutt#3rd-party-packages)
for more information

## Custom domains

Kutt supports having more than one domain. You can add domains in the custom domain section. Note
`Set domain` below is a bit misleading because it's really `Add domain`.

<center>
<img src="/img/kutt-custom-domain.png" class="shadow" width="500px">
</center>

!!! note "Ignore instruction to add A record"
    You can ignore the instruction above to add an A record to the server's IP address. Cloudron will
    add this record when you create the aliases below.

Then, in the Cloudron Dashboard add the custom domains as domain aliases in the `Location` view:

<center>
<img src="/img/kutt-domain-aliases.png" class="shadow" width="500px">
</center>

