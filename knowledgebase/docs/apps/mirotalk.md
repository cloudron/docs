# <img src="/img/mirotalk-logo.png" width="25px"> MiroTalk App

## About

Free browser based Real-time video calls. Simple, Secure, Fast.
Start your next video call with a single click. No download, plug-in, or login is required. Just get straight to talking, messaging, and sharing your screen.

* Questions? Ask in the [Cloudron Forum - MiroTalk](https://forum.cloudron.io/category/183/mirotalk)
* [MiroTalk Website](https://codecanyon.net/user/miroslavpejic85)
* [MiroTalk issue tracker](https://github.com/miroslavpejic85)

The comes in two flavors, one which relies on peer-to-peer (p2p) connections and another with a selective forwarding unit (SFU).
Depending on your use-case and networking setup one might work better than the other.

## P2P Flavor

Peer-to-peer MiroTalk

### Email Alerts

The app can optionally send email alerts to a specified email address. For this set `EMAIL_SEND_TO` in `/app/data/env` and restart the app.
```
...
EMAIL_SEND_TO=alerts@example.com
...
```

## SFU Flavor

Selective forwarding unit MiroTalk

## BRO Flavor

Broadcasting version of MiroTalk to stream videos, audio and screen from one to many.
