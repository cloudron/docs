# <img src="/img/stirling-pdf-logo.png" width="25px"> Stirling-PDF App

## About

Stirling-PDF is a powerful based PDF manipulation tool using docker that allows you to perform various operations on PDF files, such as splitting merging, converting, reorganizing, adding images, rotating, compressing, and more.

* Questions? Ask in the [Cloudron Forum - Stirling-PDF](https://forum.cloudron.io/category/168/stirling-pdf)
* [Stirling-PDF GitHub](https://github.com/Frooodle/Stirling-PDF/)
* [Stirling-PDF issue tracker](https://github.com/Frooodle/Stirling-PDF/issues)
* [Stirling Discord](https://discord.gg/Cn8pWhQRxZ)

## Custom configuration

Stirling PDF allows easy customization using [settings.yml](https://github.com/Frooodle/Stirling-PDF/#customisation).

Custom configuration can be changed in `/app/data/configs/settings.yml` using the [File manager](/apps/#file-manager). Be sure to restart the app after making any changes.

## Large PDF

Large PDF manipulation requires more memory. To fix, simply increase the [memory limit](/apps/#memory-limit) of the app.

