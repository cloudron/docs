# <img src="/img/openhab-logo.png" width="25px"> OpenHAB App

## About

The open Home Automation Bus (openHAB, pronounced ˈəʊpənˈhæb) is an open source, technology agnostic home automation platform which runs as the center of your smart home!

* Questions? Ask in the [Cloudron Forum - OpenHAB](https://forum.cloudron.io/category/152/openhab)
* [OpenHAB Website](https://www.openhab.org)
