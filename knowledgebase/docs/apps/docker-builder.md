# <img src="/img/build-service-logo.png" width="25px"> Docker Builder App

## Purpose

Cloudron can be used to build and install [custom apps](/custom-apps/tutorial/) using docker images.
Building docker images locally might require many CPU resources depending on the app. Pushing docker images
can also be network intensive (for e.g, if you are working from a coffee shop).

This app tries to solve the above situation by simply building and pushing docker images on the Cloudron where
it is installed. This app merely acts a proxy for authenticated users to build and push docker images to a configured
registry.

## Configuring CLI

Cloudron CLI can be configured to use the build service using `cloudron build --set-build-service`.
The CLI will then ask for the Cloudron credentials on which the build service is installed.

```
$ cloudron build --set-build-service
Enter build service URL: https://buildbot.example.com
Using build service https://buildbot.cloudron.ml
Building girish/nodejs-app:20191113-015207-340e7f520
Uploading source tarball...
Build Service login (https://buildbot.example.com):
Username: username
Password: *********
Login successful.
Step 1/8 : FROM cloudron/base:2.0.0@sha256:96cb00e968d7f78ff6c7f6a373ce184e0f94ad4a5298d849031201bf4a9e3bf6
 ---> 534bd0efda10
Step 2/8 : RUN mkdir -p /app/code
 ---> Running in 75e1b25ffd14
...

```

## Private registry auth

The build service requires authentication information to be able to push images to private dockerhub repositories or a private
registry. Credentials can be set by opening the [Web terminal](/apps#web-terminal)
and editing `/app/data/docker.json`. Be sure to restart the app after setting the credentials.

```
{
  "docker.io": {
    "username": "username",
    "password": "mypassword"
  },
  ...
  "custom.registry.org": {
    "username": "username",
    "password": "mypassword"
  }
}
```
