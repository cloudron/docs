# <img src="/img/sftpgo-logo.png" width="25px"> SFTPGo App

## About

SFTPGo is a full-featured and highly configurable event-driven file transfer solution.

* Questions? Ask in the [Cloudron Forum - SFTPGo](https://forum.cloudron.io/category/204/sftpgo)
* [SFTPGo Website](https://sftpgo.com)

## Custom config

Custom configuration can be put in `/app/data/sftpgo.json` using the [Web Terminal](/apps#web-terminal) or the
[File Manager](/apps/#file-manager).

After changing the file, make sure to restart the app.

