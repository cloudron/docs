 <img src="/img/chatwoot-logo.png" width="25px"> Chatwoot App

## About

Chatwoot is an open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc.

* Questions? Ask in the [Cloudron Forum - Chatwoot](https://forum.cloudron.io/category/135/chatwoot)
* [Chatwoot Website](https://www.chatwoot.com/)
* [Chatwoot docs](https://www.chatwoot.com/help-center/)
* [Chatwoot discussions](https://github.com/chatwoot/chatwoot/discussions)
* [Chatwoot issue tracker](https://github.com/chatwoot/chatwoot/issues)
* [Chatwoot Discord](https://discord.gg/cJXdrwS)

## Email Inbox Configuration

To configure Chatwoot to use Cloudron mail, add an [inbox channel](https://www.chatwoot.com/docs/product/channels/email/create-channel) of type email
with the following [IMAP](https://www.chatwoot.com/docs/product/channels/email/configure-imap) and [SMTP](https://www.chatwoot.com/docs/product/channels/email/configure-smtp) configuration:

Assuming `my.cloudron.io` is your mail server and `support@cloudron.io` is your desired support mailbox:

* Create a mailbox in your Cloudron dashboard for `support@cloudron.io`
* Set `export MAILER_INBOUND_EMAIL_DOMAIN=<mailbox-domain>` in `/app/data/env.sh` and restart the app. This is the email domain of 'forwarding inbox' inside Chatwoot. Note that you must set this value before you proceed to create an inbox inside Chatwoot.
* Add inbox channel of type email in Chatwoot
    <center>
    <img src="/img/chatwoot-create-inbox.png" class="shadow" width="500px">
    </center>
* Configure IMAP
    <center>
    <img src="/img/chatwoot-imap.png" class="shadow" width="500px">
    </center>
* Configure SMTP
    <center>
    <img src="/img/chatwoot-smtp.png" class="shadow" width="500px">
    </center>

!!! note "Forwarding Address"
    The forwarding address displayed by Chatwoot can be safely ignored.

## Custom config

[Custom environment variables](https://www.chatwoot.com/docs/self-hosted/configuration/environment-variables)
can be set in `/app/data/env.sh` using the [File manager](/apps/#file-manager). Be sure to reboot the app
after making any changes.

## Rails Console

To access the Chatwoot admin console, use the [Web terminal](/apps/#web-terminal) and source the environment
first before running rails commands.

```
# source /app/data/env.sh
# source /run/chatwoot/env.sh
# RAILS_ENV=production bundle exec rails c
```

## Custom Helpdesk Domain

To set a custom domain for Helpdesk, add [an alias](https://docs.cloudron.io/apps/#aliases) in the Cloudron dashboard:

<center>
<img src="/img/chatwoot-alias.png" class="shadow" width="500px">
</center>

Then, set the custom domain inside Chatwoot:

<center>
<img src="/img/chatwoot-helpdesk.png" class="shadow" width="500px">
</center>

