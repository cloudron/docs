# <img src="/img/etherpad-logo.png" width="25px"> Etherpad App

## Installing plugins

To install plugins or change the configuration, visit the admin interface at `/admin`.

A complete list of available plugins is available [here](https://static.etherpad.org/plugins.html).

## Admin user

Any user can be made admin.

* Adjust the `users` section in `/app/data/settings.json` via the filemanager, remove any `password` field if exist and make sure the usernames match the username on Cloudron:
```
{
  "users": {
    "username1": {
      "is_admin": true
    },
    "username2": {
      "is_admin": true
    }
  }
}
```
* Restart the app
* Relogin with that user in Etherpad. Only login via open or createing a new pad works. Login via `admin/login` does not work, as that view has no OpenID integration!
* Access `/admin`

## Custom settings

Use a [Web terminal](/apps#web-terminal) and add any custom
settings to `/app/data/settings.json`.

!!! warning ""
    The app has to be restarted after editing `/app/data/settings.json`

### Make Documents Public

By default the app will always require login with a valid user.
To allow any visitor to create and edit documents, add the following to `/app/data/settings.json`:

```
  "requireAuthentication": false,
```

## Customizing CSS

This feature was removed, but there are a few skin variants available, like the dark mode mentioned below.
See the [etherpad docs](https://etherpad.org/doc/latest/#index_skins) for more information.

### Dark mode

The app ships with the **colibris** theme/skin. This skin supports a dark mode through the [skinVariants](https://github.com/ether/etherpad-lite/blob/develop/settings.json.template#L93). To enable that, edit `/app/data/settings.json`:

```
  "skinVariants": "super-dark-toolbar super-dark-editor dark-background",
```

## API Access

The [Etherpad API](http://etherpad.org/doc/v1.3.0/#index_http_api) can be accessed by
obtaining the APIKEY. For this, open a [Web terminal](/apps#web-terminal)
and view the contents of the file `/app/data/APIKEY.txt`.

Example usage:

    curl https://etherpad.domain/api/1.2.7/listAllPads?apikey=c5513793f24a6fbba161e4497b26c734ff5b2701fad0f1211097ccb405ea65c7

## Troubleshooting

If the app does not start, especially after an update, usually this is related to incompatible plugins.
To fix this situation, put the app in recovery mode and open a [Web terminal](/apps#web-terminal).
Get a list of installed plugins:
```
npm ls 2> /dev/null | grep ep_
```
The two plugins `ep_cloudron` and `ep_etherpad-lite` are required, any other plugin might cause the issue.
Uninstall other plugins one by one with:
```
npm rm <pluginname>
```
Then see if the app can start up again by running `/app/pkg/start.sh`. If the app starts up and is accessible normally,
disable recovery mode again, otherwise try the next one.

Plugins which did not cause the problem can be reinstalled again with:
```
npm i <pluginname>
```
