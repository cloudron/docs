# <img src="/img/peertube-logo.png" width="25px"> PeerTube App

## About

PeerTube is an activityPub-federated video streaming platform using P2P directly in your web browser.

* Questions? Ask in the [Cloudron Forum - PeerTube](https://forum.cloudron.io/category/91/peertube)
* [PeerTube Website](https://joinpeertube.org/)
* [PeerTube issue tracker](https://github.com/Chocobozzz/PeerTube/issues)

## Customization

Use the [File manager](/apps#file-manager) to edit custom configuration under `/app/data/production.yaml`.

## CLI

The CLI can be accessed using the `peertube-cli` command. You can view the various commands in the [PeerTube docs](https://docs.joinpeertube.org/maintain/tools#cli-wrapper).

### Uploading video

```
# peertube-cli up --file /tmp/video.wmv --url https://peertube.cloudron.club --username root --password changeme --video-name "Sample video"
Uploading Sample video video...
Video Sample video uploaded.
```

## Disable P2P

If playback is slow, it might be a good idea to disable the P2P functionality. For this, use the file [File manager](/apps/#file-manager) to edit `/app/data/production.yaml` and set tracker to false.

```
tracker:
  # If you disable the tracker, you disable the P2P aspect of PeerTube
  enabled: false
```

