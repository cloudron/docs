# <img src="/img/findmydevice-logo.png" width="25px"> FindMyDevice App

## About

FindMyDevice (FMD) Server finds your device and control it remotely. It aims to be a secure open source alternative to Google's Find My Device.

The FMD app for android can be found [here](https://f-droid.org/packages/de.nulide.findmydevice/) ([source](https://gitlab.com/Nulide/findmydevice)].

* Questions? Ask in the [Cloudron Forum - FindMyDevice](https://forum.cloudron.io/category/199/findmydevice)
* [FindMyDevice repo](https://git.cloudron.io/apps/findmydevice-app)
* [FindMyDevice issue tracker](https://gitlab.com/Nulide/findmydeviceserver/-/issues)

## Custom Config

Custom config can be set in `/app/data/config.yml` using the [File manager](/apps/#file-manager).

Be sure to restart the app after making any changes.

## UnifiedPush

Communication between server and app relies on [UnifiedPush](https://unifiedpush.org/). The push can be either be selfhosted using
[ntfy.sh](/apps/ntfy) or use the service at https://ntfy.sh . If you decide to selfhost ntfy server, you must enable support for
UnifiedPush. See https://docs.ntfy.sh/config/#example-unifiedpush
