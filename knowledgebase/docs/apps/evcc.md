# <img src="/img/evcc-logo.png" width="25px"> evcc App

## About

evcc is an energy management system with a focus on electromobility. The software controls your EV charger or smart plug. It communicates with your vehicle, inverter or home storage to make intelligent charging decisions.

* Questions? Ask in the [Cloudron Forum - evcc](https://forum.cloudron.io/category/194/evcc)
* [evcc repo](https://github.com/evcc-io/evcc)
* [evcc issue tracker](https://github.com/evcc-io/evcc/issues)
