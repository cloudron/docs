# <img src="/img/keycloak-logo.png" width="25px"> Keycloak App

## About

Keycloak is an Open Source Identity and Access Management.

* Questions? Ask in the [Cloudron Forum - Keycloak](https://forum.cloudron.io/category/203/keycloak)
* [Keycloak Website](https://www.keycloak.org/)
* [Keycloak issue tracker](https://github.com/keycloak/keycloak/issues)
* [Keycloak community](https://www.keycloak.org/community)

## Features

Keycloak has packed some functionality in [features](https://www.keycloak.org/server/features). It is
not possible to add or remove features at runtime since this is a build time option and builds a new binary.

The following features are enabled in the Cloudron package:

* authorization
* account:v3
* account-api
* impersonation
* client-policies
* passkeys (preview, may change of be removed in a future release)

