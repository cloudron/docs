# <img src="/img/baserow-logo.png" width="25px"> Baserow App

## About

Baserow is an open source no-code database tool and Airtable alternative.

* Questions? Ask in the [Cloudron Forum - Baserow](https://forum.cloudron.io/category/136/baserow)
* [Baserow website](https://baserow.io/)
* [Baserow community](https://community.baserow.io/)
* [Baserow issue tracker](https://gitlab.com/bramw/baserow/-/issues)

## Custom configuration

[Custom env variables](https://baserow.io/docs/installation/configuration) can be set in the file `/app/data/env.sh` using the [File manager](/apps/#file-manager).

Be sure to restart the app after making any changes.

## Registration

To disable public registration, `Admin` -> `Settings` -> uncheck `Allow creating new accounts` .

## Log Retention

Baserow stores audit logs in the database and this can consume a lot of space. This can be configured using
two variables in `/app/data/env.sh`:

```
export BASEROW_ROW_HISTORY_RETENTION_DAYS=365
export BASEROW_ENTERPRISE_AUDIT_LOG_RETENTION_DAYS=30 # set this even if not enterprise customer
```

Be sure to restart the app after making any changes.
