# <img src="/img/uptime-kuma-logo.png" width="25px"> Uptime Kuma App

## About

Uptime Kuma is a self-hosted monitoring tool like "Uptime Robot".

* [Website](https://github.com/louislam/uptime-kuma)
* [Issue tracker](https://github.com/louislam/uptime-kuma/issues)

## Custom config

[Custom environment variables](https://github.com/louislam/uptime-kuma/wiki/Environment-Variables)
can be set by editing `/app/data/env` using the [File manager](/apps/#file-manager). Be sure to restart the app after making
any changes.

Example:

```
export UPTIME_KUMA_DISABLE_FRAME_SAMEORIGIN=true
```

## Reset Password

To reset the password, open a [Web Terminal](https://docs.cloudron.io/apps/#web-terminal) and run:

```
# gosu cloudron:cloudron npm run reset-password

> uptime-kuma@1.23.16 reset-password
> node extra/reset-password.js

== Uptime Kuma Reset Password Tool ==
Connecting the database
2025-01-01T13:55:10+00:00 [DB] INFO: Data Dir: ./data/
Found user: girish
New Password: 
```

## Remove 2FA

To remove the 2FA, open a [Web Terminal](https://docs.cloudron.io/apps/#web-terminal) and run:

```
# gosu cloudron:cloudron npm run remove-2fa
```

