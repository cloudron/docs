# Notifications

## Overview

Cloudron will display notifications in the dashboard about various events like:

* If app goes down
* If app run out of memory
* Low disk space
* Updates available
* App updated

The notifications can be read by clicking on the icon in the navigation bar.

<center>
<img src="/img/notifications-icon.png" class="shadow" width="500px">
</center>

<center>
<img src="/img/notifications-view.png" class="shadow" width="500px">
</center>

## Email Notifications

Email notifications can be configured per user in the `Notifications` view.

<center>
<img src="/img/notifications-settings-button.png" class="shadow" width="500px">
</center>

<center>
<img src="/img/notifications-settings-dialog.png" class="shadow" width="500px">
</center>

## Server Health Check

For reliability, it is best to configure an external service to monitor the healthcheck
of Cloudron itsef. Use `https://my.<domain>/api/v1/cloudron/status` as the health check URL.

